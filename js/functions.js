/*$('#password, #confirm_password').on('keyup', function () {
  if ($('#password').val() == $('#confirm_password').val()) {
    $('#message').html('Matching').css('color', 'green');
  } else
    $('#message').html('Not Matching').css('color', 'red');
});*/


function controlla(){
  $("#password, #password-confirmation").keyup(function() {
    if ($("#password").val() == $("#password-confirmation").val()) {
      $("#password-confirmation").css('border-color', '#1bc21b');
      $("#password").css('border-color', '#1bc21b');
    } else {
    $("#password").css('border-color', '#f22e2e');
    $("#password-confirmation").css('border-color', '#f22e2e');
  }});
}

//Limita l'input in una casella di testo a soli numeri, tramite il codice ascii dei caratteri consentiti
function onlyNumberKey(event) {
    var ASCIICode = (event.which) ? event.which : event.keyCode
    if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57))
        return false;
    return true;
}
//Formatta il testo nella inputbox per separare il numero della carta in blocchi da 4 cifre
function formattaCarta(){
  $('#numerocarta').on('keypress change', function () {
    $(this).val(function (index, value) {
      return value.replace(/\W/gi, '').replace(/(.{4})/g, '$1 ');
    });
  });
}

/*Come con filtraAnno, cambia il bordo delle inputbox selezionate per mostrare quali
mesi e quali anni NON sono validi*/
function filtraMese(){
  $("#mesescadenza").keyup(function(){
    if(($("#mesescadenza").val() > 12) || ($("#mesescadenza").val() < 1)){
      $("#mesescadenza").css('border-color', '#f22e2e');
    } else {
      $("#mesescadenza").css('border-color', '#d3d3d3');
    }
  });
}

function filtraAnno(){
  $("#annoscadenza").keyup(function(){
    if(($("#annoscadenza").val() < 2022) || ($("#annoscadenza").val() > 2026)){
      $("#annoscadenza").css('border-color', '#f22e2e');
    } else {
      $("#annoscadenza").css('border-color', '#d3d3d3');
    }
  });
}
