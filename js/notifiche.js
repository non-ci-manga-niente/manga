$(document).ready(function () {
    var bell = "fa fa-bell fa-2x";
    var exscalamation = "fa fa-lightbulb-o fa-2x";
    $("#bell").attr("class", bell);
    function doPoll(){
        $.get('./utils/new-notification.php', function(data) {
            if (data[1] == "]") {
                $("#bell").attr("class", bell);
                $('.toast').toast('hide');
            } else {
                $("#bell").attr("class", exscalamation);
                var dat = JSON.parse(data);
                out="";
                dat.forEach(function(item){
                  if(item["type"] == 0){
                    out = out + "Il prodotto " + item["nome"] + " è stato acquistato!" + "</br>";
                  }
                  else if(item["type"] == 1){
                    out = out + "Il prodotto " + item["nome"] + " ha esaurito le scorte!" + "</br>";
                  }
                  else if(item["type"] == 2){
                    out = out + "Il prodotto " + item["nome"] + " è stato spedito!" + "</br>";
                  }
                  else if(item["type"] == 3){
                    out = out + "L'acquisto di " + item["nome"] + " è stato disdetto!" + "</br>";
                  }
                  else if(item["type"] == 4){
                    out = out + "Il prodotto " + item["nome"] + " è arrivato a destinazione!" + "</br>";
                  }
                })
                document.getElementById("tosttext").innerHTML = out;
                $('.toast').toast('show');
            }
            setTimeout(doPoll, 1000);
        });
    }
    doPoll();

    $("#tostbtn").click(function(e){
      e.preventDefault();
      $.get('./utils/update-notifications.php');
    })
});
