<?php
  require_once 'bootstrap.php';

  if(!isset($_SESSION["id"])){
    header("location: login.php");
  }

  $userid= $_SESSION["id"];
  if(!empty($_POST)){
    if($_POST["operation"] == 0){ //Conferma l'arrivo del prodotto
      $dbh->deleteOrdini($_POST["order"]);
      $seller = $dbh->getSellerOfArticle($_POST["art"]);
      $dbh->addNotification($_SESSION["id"], $seller[0]["userid"], $_POST["art"], 4, $_POST["quantity"]);
    }
    elseif (($_POST["operation"] == 1)) { //Disdici l'acquisto
      $dbh->deleteOrdini($_POST["order"]);
      $dbh->incQuantita($_POST["art"], $_POST["quantity"]);
      $seller = $dbh->getSellerOfArticle($_POST["art"]);
      $dbh->addNotification($_SESSION["id"], $seller[0]["userid"], $_POST["art"], 3, $_POST["quantity"]);
    }
    elseif (($_POST["operation"] == 2)) { //Spedisci
      $dbh->statusUpdate($_POST["order"], 2);
      $dbh->addNotification($_POST["client"], $_SESSION["id"], $_POST["art"], 2, $_POST["quantity"]);
    }
  }
  require 'orders.php';
?>
