<?php
  require_once 'bootstrap.php';

  if(isset($_POST["username"]) && isset($_POST["password"])){
      $login_result = $dbh->processLogin($_POST["username"], $_POST["password"]);
      if($login_result == NULL){
          $templateParams["login-status"] = "Errore: controllare email o password";
          deleteSession();
      }
      else{
          registerLoggedUser($login_result);
          $templateParams["login-status"] = "Login avvenuto con successo";
          //header("location: index.php");
      }
  }

  if(isUserLoggedIn()){
    $templateParams["titolo"] = "Profilo";
    $templateParams["nome"] = "account.php";
    require 'template/base.php';
  }
  else {
    $templateParams["titolo"] = "Login";
    $templateParams["nome"] = "login-page.php";
    require 'template/login-page.php';
  }

?>
