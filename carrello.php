<?php
require_once 'bootstrap.php';
if(!isset($_SESSION["id"])){
  header("location: login.php");
}
$userid= $_SESSION["id"];

if ($_POST["operation"]==1) {
  $idarticolo = $_POST["ids"];
  $quantity = $_POST["quantity"];
  $item = $dbh->getArticleById($idarticolo);
  $carttmp = $dbh->getCartByUserId($userid);
  $found = 0;
  if(isset($carttmp[0])){
    foreach ($carttmp as $incart) {
      if($incart["articleid"] == $idarticolo){
        $found = 1;
        $dbh->SetQuantCart($incart["orderid"],$quantity);
      }
    }
  }
  if($found == 0){
    $dbh->addToCart($userid,$idarticolo,$item[0]["prezzo"], $quantity);
  }
  $usercart = $dbh->getCartByUserId($userid);
}
if($_POST["operation"]==2){
  $usercart = $dbh->getCartByUserId($userid);
}
if($_POST["operation"]==4){
  $incart = $dbh->getCartByUserId($userid);
  foreach ($incart as $article) {
    $dbh->insertBought($article["articleid"], $article["orderid"]);
  }
  $seller = $dbh->getSellerOfArticle($incart[0]["articleid"]);
  $dbh->addNotification($incart[0]["userid"], $incart[0]["userid"], $incart[0]["orderid"]);
}
/*(clientid, sellerid, cartid)
if ($usercart==NULL) {
  $usercart=NULL;
}*/
$totale =0;
$templateParams["titolo"] = 'Carrello';
$templateParams["nome"] = 'carrello.php';
require 'template/base.php';
?>
