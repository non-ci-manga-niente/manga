# Non Ci Manga Niente
Non Ci Manga Niente è un'implementazione di un semplice sito di e-commerce.
In questa piattaforma gli studenti possono ordinare dei fumetti e manga e attendere l'arrivo in uno spot del campus di Cesena.
I venditori possono facilmente aggiungere prodotti in vendita e gestire i propri.
Inoltre, la piattaforma spedisce notifiche sia agli studenti che ai venditori quando un ordine viene creato o spedito.
Per utilizzare la piattaforma basta creare un account dall'apposita pagina, poi si potrà accedere alla Home dove è presente la lista dei prodotti acquistabili.
Selezionandone uno è facile acquistarlo dal proprio Carrello e procedere inserendo i dati di una carta di credito.
Si può accedere alla pagina degli Ordini per seguire la progressione del proprio acquisto.
Analogamente per i venditori bisogna creare un account Venditore per poi accedere alla pagina dell'account, dove ci sarà una funzionalità solo per loro che permetterà
di inserire nuovi prodotti per permettere ad altri di acquistarli o ricaricare le scorte di prodotti già in vendita.
I venditori riceveranno una notifica quando un loro prodotto viene acquistato e possono gestire la spedizione tramite la pagina degli Ordini.
