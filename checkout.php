<?php
require_once 'bootstrap.php';
$templateParams["titolo"] = "Checkout";
$templateParams["nome"] = "form-pagamento.php";

if(isset($_POST["mesescadenza"]) && isset($_POST["annoscadenza"]) && isset($_POST["cvv"])
 && isset($_POST["numerocarta"]) && isset($_POST["titolare"])){
  if($_POST["mesescadenza"] < 1 || $_POST["mesescadenza"] > 12 || $_POST["annoscadenza"] < 2022 || $_POST["annoscadenza"] > 2026){
    $templateParams["validitacarta"] = "La data di scadenza fornita non è valida";
  } else {
    header("location: compra.php");
  }
}
require 'template/base.php';
?>
