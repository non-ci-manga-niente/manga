<?php
  require_once 'bootstrap.php';

  if(!isset($_SESSION["id"])){
    header("location: login.php");
  }
  $userid= $_SESSION["id"];
  if($_SESSION["Type"]=="admin"){
    $templateParams["notifica"] = $dbh->getSells($userid);
  }

  if($_SESSION["Type"]=="cliente"){
    $templateParams["notifica"] = $dbh->getBought($userid);
  }
  $templateParams["titolo"] = "Ordini";
  $templateParams["nome"] = 'orders.php';
  require 'template/base.php';
?>
