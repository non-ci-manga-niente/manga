<?php
  require_once 'bootstrap.php';

  if(!isset($_SESSION["id"])){
    header("location: login.php");
  }
  $userid= $_SESSION["id"];
  if($_SESSION["Type"]=="admin"){
    $templateParams["notifica"] = $dbh->getNotificationAdminAll($userid);
    $dbh->NotificationUpdateAdmin($userid);
    $dbh->NotificationUpdate($userid);
  }

  if($_SESSION["Type"]=="cliente"){
    $templateParams["notifica"] = $dbh->getNotificationAll($userid);
    $dbh->NotificationUpdate($userid);
  }
  $templateParams["titolo"] = "Notifiche";
  $templateParams["nome"] = 'notifiche.php';
  require 'template/base.php';
?>
