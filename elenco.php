<?php
require_once 'bootstrap.php';

$templateParams["articoli"] = $dbh->getArticleOfSeller($_SESSION["id"]);
$templateParams["titolo"] = "Modifica Listino";
$templateParams["nome"] = 'elenco.php';
if(isset($_COOKIE["error"])){
  $error = $_COOKIE["error"];
  setcookie("error", "1", time()- 60,'/');
}

require 'template/base.php';
?>
