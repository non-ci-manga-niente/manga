<?php
  require_once 'bootstrap.php';

  if(isset($_POST["name"]) && isset($_POST["username"]) && isset($_POST["password"]) && isset($_POST["password-confirmation"])){
    if($_POST["password"] == $_POST["password-confirmation"]){

      $name = $_POST["name"];
      $username = $_POST["username"];
      $password = $_POST["password"];
      $usertype = $_POST["Type"];
      $username_check = $dbh->checkUsername($username);
      if($username_check){
          //Email o username già in uso
          $templateParams["signup-status"] = "Errore: email già in uso";
      }
      else{

        $signup_result = $dbh->insertUser($name, $username, $password, $usertype);

        if($signup_result){
          $login_result = $dbh->processLogin($username, $password, $usertype);
          registerLoggedUser($login_result);
          $templateParams["signup-status"] = "Registrazione avvenuta con successo";
          header("location: index.php");
        }
        else{
          $templateParams["signup-status"] = "Qualcosa è andato storto";
        }
      }
    }
    else{
      $templateParams["signup-status"] = "Errore! Le due password non combaciano.";
    }
  }

  require 'template/signup-page.php';
?>
