-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Feb 04, 2022 alle 09:25
-- Versione del server: 10.4.14-MariaDB
-- Versione PHP: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `manga`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `articolo`
--

CREATE TABLE `articolo` (
  `idarticolo` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `descr` mediumtext NOT NULL,
  `prezzo` int(11) NOT NULL,
  `img` varchar(100) NOT NULL,
  `quantity` int(11) NOT NULL,
  `visible` int(1) NOT NULL,
  `autore` text NOT NULL,
  `anno` year(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `articolo`
--

INSERT INTO `articolo` (`idarticolo`, `nome`, `descr`, `prezzo`, `img`, `quantity`, `visible`, `autore`, `anno`) VALUES
(14, 'Sakuya Adventure', 'Sakuya dovrà lanciare i suoi coltelli contro i vari invasori che nei prossimi giorni arriveranno alla magione scarlatta', 15, 'Izayoi.Sakuya.full.1981946.jpg', 0, 1, '', 0000),
(44, 'Attack on Titan vol. 1', 'Con l\'avvento dei giganti l\'umanità è stata decimata e si è ritirata a vivere nel territorio compreso all\'interno di tre imponenti mura concentriche: il Wall Maria, il Wall Rose e il Wall Sina, dove è stata al riparo dai giganti per cento anni. Un giorno, un gigante colossale e uno corazzato creano una breccia nelle mura presso la città di Shiganshina. Il territorio tra il Wall Maria e il Wall Rose viene così invaso dai giganti con la conseguente morte di migliaia di persone. In seguito all\'uccisione della madre da parte di un gigante, Eren Jaeger, uno dei superstiti di Shiganshina, giura vendetta e si arruola nell\'esercito, insieme a sua sorella adottiva Mikasa Ackermann e al loro amico Armin Arelet.', 5, 'L\'attacco_dei_giganti_copertina.jpeg', 10, 1, 'Hajime Isayama', 2010),
(45, 'Attack on Titan vol. 9', 'L\'Armata Ricognitiva si appresta a far evacuare i villaggi vicini. Un gigante bestiale, che è in grado di parlare, fa divorare il caposquadra Mike dagli altri giganti di cui è a capo. Nel suo villaggio natale, Sasha salva una bambina e si ricongiunge con suo padre. Anche Connie raggiunge il proprio villaggio, che però è già stato distrutto dai giganti, tuttavia non c\'è traccia di sangue ed un gigante che somiglia a sua madre lo saluta. Studiando un frammento del cristallo di Annie, Hansie ha l\'idea di sigillare la breccia usando la forma gigante di Eren, se egli imparerà ad utilizzare questo potere. Il reverendo Nick le rivela che uno dei membri dell\'Armata Ricognitiva detiene la chiave del segreto delle mura: Christa Lens. I soldati, nonostante percorrano il perimetro del Wall Rose, non trovano alcuna breccia. Per ripararsi dai giganti, si rifugiano allora nel vicino castello di Utgard, verso cui sono diretti anche Eren e gli altri.', 6, 'shingeki-no-kyojin-el-ataque-de-los-titanes-vol-9.jpg', 9, 1, 'Hajime Isayama', 2012),
(46, 'Attack on Titan vol. 11', 'Faccia a faccia con i responsabili della caduta di Shiganshina, Eren affronta il Gigante Corazzato, mentre il Gigante Colossale si difende dagli altri soldati. Usando le mosse che ha imparato da Annie, e con l\'aiuto di Mikasa, Eren immobilizza Reiner ma il gigante colossale atterra su di lui in un\'esplosione. Ymir ed Eren vengono dunque catturati. Cinque ore dopo, Mikasa e gli altri si svegliano dallo shock e sono raggiunti da Elvin e altri rinforzi, che organizzano un inseguimento. Presso la foresta degli alberi giganti, i guerrieri Reiner e Berthold attendono che giunga la notte per poter fare ritorno al loro paese natio. Ymir ed Eren scoprono così che Reiner ha uno sdoppiamento di personalità, per poter sopportare il peso delle proprie azioni. Reiner convince Ymir a non fuggire da loro, in cambio della garanzia di proteggere almeno Christa.', 6, 'SNK_Manga_Volume_11.png', 11, 1, 'Hajime Isayama', 2013),
(47, 'Miss Kobayashi\'s Dragon Maid vol. 1', 'Kobayashi è impiegata alla InferNet corporation come programmatrice e segretamente appassionata della figura della cameriera, in particolare dell\'epoca vittoriana. La sua vita cambia radicalmente quando, tornando a casa dopo una sbronza, si perde sui monti e incontra un drago in punto di morte. Kobayashi lo salva e il drago, assunta forma umana, si rivela essere una giovane femmina a cui Kobayashi offre, ancora ubriaca, ospitalità.\r\n\r\nIl giorno dopo, Kobayashi, non ricordando gli eventi della notte precedente, si ritrova un drago davanti a casa sua, che diventa una ragazza con corna e coda di nome Tohru. Dopo un breve riassunto e un momento di diffidenza, Kobayashi decide di tenerla come cameriera.', 5, '51qv2IdWLzL.jpg', 8, 1, 'Cool-kyō Shinja', 2014),
(48, 'Miss Kobayashi\'s Dragon Maid vol. 2', 'Dalle pulizie a Comiket fino a una gita in spiaggia, Dragon Maid Tohru è pronta ad aiutare la sua amata Miss Kobayashi con tutta la sua forza soprannaturale! Tohru sta iniziando ad abituarsi alla vita nel mondo umano, così come i suoi amici draghi, come la graziosa piccola Kanna e l\'oscuro e lunatico Fafnir. Ma cosa accadrà quando suo padre apparirà per riportarla nel vecchio mondo?! Scoprilo in questa commedia amata sia dagli umani che dai draghi!', 6, '51DjN7NozaS._SX347_BO1,204,203,200_.jpg', 6, 1, 'Cool-kyō Shinja', 2015),
(49, 'Miss Kobayashi\'s Dragon Maid vol. 3', 'Tohru il drago si è trovata molto a suo agio nel suo ruolo di cameriera di Miss Kobayashi. Ma quando Elma, il nuovo drago in città, entra in scena, la gelosia di Tohru trabocca! Le battaglie per il pranzo al sacco e i vicini rumorosi non sono niente che Tohru non può gestire quando l\'affetto della sua cara Miss Kobayashi è in gioco!', 6, '81rEK+QDzLL.jpg', 7, 1, 'Cool-kyō Shinja', 2015),
(50, 'Tokyo Ghoul vol. 1', 'Il timido Ken Kaneki è felice di aver ottenuto un appuntamento con la bellissima Rize Kamishiro. Scopre tuttavia che Rize, un ghoul, è solo intenzionata a mangiarlo. Dopo essere stato salvato tramite un\'operazione dalla moralità ambigua per mano del dottor Kanou, scopre di essere diventato un mezzo ghoul, un ibrido in parte umano e in parte ghoul. Da quel momento, dovrà fare i conti con il violento e spietato mondo dei ghoul.', 5, '71cZe3b5sjL._AC_SY679_.jpg', 2, 1, 'Sui Ishida', 2012),
(51, 'Tokyo Ghoul vol. 2', 'Incapace di disfarsi della sua umanità, ma allo stesso tempo incapace di sopprimere la fame, Kaneki viene aiutato dai ghoul dell\'Anteiku, che gli insegnano a mescolarsi nella società umana. Tuttavia, alcuni recenti avvenimenti attirano l\'attenzione della Commissione Anti Ghoul, che non discrimina tra ghoul violenti e pacifici.', 6, '61ulqCrvJ4L.jpg', 5, 1, 'Sui Ishida', 2012),
(52, 'Naruto vol. 1', 'Dal settimanale “Shonen Jump” (Shueisha) ecco “Naruto”, incentrato sulle avventure di un ragazzino, Uzumaki Naruto, che vuole diventare un famoso ninja e lasciare un segno nella storia. Gli ostacoli sul suo cammino sono però molti, come le prove che dovrà sostenere e i rivali che dovrà affrontare… Testi e disegni del giovane e talentuoso Masashi Kishimoto, che si è affermato proprio con questo manga grazie a storie che mescolano azione e umorismo, disegnate con un tratto dinamico e sicuro.', 5, '912xRMMra4L.png', 7, 1, 'Masashi Kishimoto', 1999),
(53, 'Naruto vol. 8', 'I ventuno componenti rimasti dovranno affrontare individualmente la terza prova: un torneo di combattimento a eliminazione diretta. Vedremo scontrarsi Sasuke con Yoroi, Zaku con Shino, Tsurugi con Kankuro e Sakura con Ino.', 6, '91wCSK7QalL.png', 6, 1, 'Masashi Kishimoto', 2018),
(54, 'Naruto vol. 11', 'Sotto la supervisione di un nuovo maestro d’eccezione, Naruto esercita la sua capacità di controllo del chakra in vista dei difficili scontri che lo attendono nella terza prova dell’esame di selezione dei chunin', 5, '91K2tr5N6nL.png', 1, 1, 'Masashi Kishimoto', 2020),
(55, 'One Piece vol. 1', 'Monkey D. Rufy, un bambino di sette anni, un giorno mangia il frutto del diavolo Gom Gom che gli fa diventare il corpo di gomma, ma che lo rende incapace di nuotare. In seguito a una peripezia col masnadiero Higuma, Rufy finisce in mare, ma viene salvato dal suo amico pirata Shanks, che perde, però, il braccio sinistro. Rufy gli promette allora di diventare il Re dei pirati guadagnandosi come regalo il cappello di paglia dell\'amico. Dieci anni dopo infatti, Rufy si imbarca per mare con l\'obbiettivo di diventare il Re dei pirati alla ricerca del leggendario tesoro lasciato da Gold Roger, lo One Piece. Dopo essere naufragato e sconfitto la piratessa Albida, parte insieme al suo mozzo Kobi per l\'isola seguente. Qui è imprigionato Roronoa Zoro, un famoso spadaccino e cacciatore di taglie, torturato da Hermeppo, il figlio del capitano della Marina Morgan mano d\'ascia. Dopo la sconfitta di Morgan, Zoro entra nell\'equipaggio di Rufy, e Kobi nella Marina. Rufy viene trasportato da un uccello su un\'isola dove la ladra Nami era inseguita da alcuni pirati perché aveva rubato la mappa della Rotta Maggiore. Rufy sconfigge i tre pirati e Nami gli propone di lavorare insieme.', 6, '61MxIOS2GVL._SX342_SY445_QL70_ML2_.jpg', 14, 1, 'Eiichirō Oda', 1997),
(56, 'One Piece vol. 2 VERSUS!! Bagī kaizoku-dan', 'Rufy accetta l\'invito di Nami a collaborare e così lei lo porta dalla ciurma di Bagy. Con uno stratagemma la ragazza tenta quindi di unirsi all\'equipaggio di Bagy, con l\'intenzione di rubare i suoi tesori, ma si rifiuta di sparare a Rufy e viene accusata di tradimento. Proprio in quel momento, Zoro arriva per salvare il suo capitano e taglia a metà Bagy. Tuttavia, grazie all\'abilità del frutto del diavolo Puzzle Puzzle, Bagy sopravvive e con un pugnale, colpisce Zoro alle spalle. Nonostante sia ferito, lo spadaccino scappa portando la gabbia con dentro Rufy sulle spalle. Bagy invia il domatore di bestie Moji all\'inseguimento dei due. Rufy, dopo essersi liberato, sconfigge Moji. In seguito, Rufy e Zoro affrontano la ciurma del pirata pagliaccio. Zoro sconfigge lo spadaccino acrobata Kabaji mentre Rufy intraprende uno scontro con Bagy.', 7, '61mPXYW0vdL.jpg', 6, 1, 'Eiichirō Oda', 1998),
(57, 'One Punch Man vol. 1', 'In un mondo dove esistono mostri ed eroi, Saitama, un venticinquenne diventato calvo e fortissimo a seguito di un duro allenamento, ha deciso di diventare un eroe per hobby, ma neanche ciò riesce a ravvivare la sua apatia perché nessun nemico è alla sua altezza: tutti vanno al tappeto con un pugno. Non avendo rivali, Saitama prosegue ad eliminare i mostri che incontra, sperando un giorno di trovare un degno avversario.', 8, 'One-Punch_Man.jpg', 6, 1, 'One', 2012),
(58, 'One Punch Man vol. 2', 'Saitama viene coinvolto suo malgrado in uno scontro tra una strana creatura femminile dall\'aspetto di una zanzara umanoide, Mosquito Girl, ed un giovane cyborg di nome Genos. La creatura semi-animalesca è sul punto di annientare il suo avversario quando Saitama la sconfigge con un solo schiaffo. Genos, impressionato, gli chiede di divenire suo allievo e Saitama, noncurante, accetta. Genos gli spiega in seguito di aver perso la sua famiglia a causa di un cyborg anni prima e di essersi fatto trasformare lui stesso in un cyborg per avere il potere di vendicarsi, senza però aver ancora trovato il responsabile della sua tragedia. I due vengono interrotti dall\'arrivo di altri esseri ibridi, tra cui vi è anche un cyborg, che hanno l\'ordine di portare Saitama dal loro creatore. L\'eroe pelato sconfigge facilmente quasi tutti i nemici incluso \"Beast King\", un leone umanoide, mentre Genos riesce a battere \"Armored Gorilla\", il cyborg, e si appresta ad interrogarlo.', 7, 'download.jpg', 7, 1, 'One', 2012),
(59, 'One Punch Man vol. 4', 'La Città Z, dove vive Saitama, corre un grave pericolo e gli eroi di Classe S vengono richiamati per annientare la minaccia: un enorme meteorite sta per abbattersi sulla città e distruggerla. I cittadini vengono fatti evacuare, ma tra gli eroi di Classe S si presentano solo Metal Knight, Genos e Bang, l\'anziano, ma formidabile, numero 3 nella graduatoria della Classe S. Bang è un artista marziale letale, quindi non è utile allo scopo. I missili di Metal Knight si rivelano inefficaci, perciò Genos usa tutta la sua potenza per cercare di distruggere il meteorite, fallendo. Giunge allora Saitama, che salta e fa a pezzi il meteorite con un pugno. I frammenti devastano numerose zone della città, ma senza vittime e con danni molto inferiori rispetto a quelli che si sarebbero verificati col meteorite intero. Saitama, per questo, viene promosso dalla 342ª posizione alla 5ª posizione della Classe C, mentre Genos alla 14ª della S, poiché parte del merito viene erroneamente attribuito a lui e a Metal Knight. Giorni dopo, due eroi, Tanktop Tiger, 6° di Classe C, e Tanktop Black Hole, 89° di Classe B, accusano Saitama di essersi preso il merito per il meteorite distrutto e di essere un imbroglione che ha causato i danni alla città, portando la folla ad accusarlo a sua volta. I due cercano poi di fare bella figura attaccandolo, ma Saitama li batte come se niente fosse. Dopodiché l\'eroe calvo insulta la gente affermando che fa l\'eroe solo perché lo desidera, non per essere ammirato, perciò che loro lo odino o meno non gli interessa.', 8, 'mmaon025r_0_lwf65szare9pjf5w.jpg', 6, 1, 'One', 2013),
(60, 'Jojo\'s Bizzarre Adventure - Phantom Blood', 'Liverpool, Inghilterra di fine XIX secolo. Jonathan Joestar è l\'unico erede del casato dei Joestar, il cui maggiore esponente è suo padre, il ricco lord George Joestar I. Questi adotta Dio Brando, coetaneo di Jonathan e figlio di Dario Brando, uno sbandato che anni prima aveva tentato di derubarlo a seguito di un incidente e che lui crede gli abbia salvato la vita, in segno di riconoscimento dopo la sua morte per malattia. Dio, la cui vera indole è ambiziosa e perversa, vuole in realtà impadronirsi della fortuna dei Joestar, cercando nei primi mesi di convivenza di distruggere la vita del fratello adottivo e, sette anni dopo, di uccidere il padre adottivo con lo stesso veleno che aveva utilizzato per uccidere il proprio padre violento e abusivo. Scoperto e messo alle strette da Jonathan e dal brigante di strada Robert E.O. Speedwagon, Dio usa su di sé la \"Maschera di Pietra\", un portentoso artefatto di origine azteca acquistato in passato dai Joestar in grado di trasformare un qualunque umano in un vampiro immortale, il cui unico punto debole è la luce solare. Per affrontarlo, Jonathan apprende la tecnica delle \"onde concentriche\" dal misterioso avventuriero Will Antonio Zeppeli, anch\'egli alla ricerca della maschera per distruggerla, grazie alla quale può scontrarsi con zombie e cavalieri medievali evocati da Dio Brando fino allo scontro finale con il nemico: qui JoJo risulta vittorioso e, credendo di aver vendicato il padre e gli alleati caduti contro il vampiro, riesce finalmente a sposarsi con l\'amica d\'infanzia Erina Pendleton e a partire con lei in luna di miele per l\'America. Sulla nave irrompe però il redivivo Dio, che per sopravvivere era riuscito a separare la testa dal corpo ormai inutilizzabile; JoJo muore nella lotta, mentre il battello viene distrutto da un\'esplosione, da cui sopravvivono Erina e una bambina da lei salvata, ritrovati due giorni dopo a bordo di una bara rimasta intatta nell\'incidente.', 9, '818fOuBHTnL.jpg', 5, 1, 'Hirohiko Araki', 1993),
(61, 'Jojo\'s Bizzarre Adventure - Stardust Crusaders', 'La storia prende parte nel 1988 con la notizia che Dio Brando, dato per morto dopo lo scontro finale con Jonathan Joestar al termine della prima serie, è in realtà ancora vivo, perché la sua testa, staccatasi dal corpo, riuscì negli ultimi attimi prima dell\'esplosione ad appropriarsi del corpo di Jonathan per poi rinchiudersi nel doppio fondo della bara utilizzata da Erina per salvarsi e sprofondare nell\'abisso marino. Quasi cento anni dopo la bara viene recuperata e portata in superficie da un gruppo di incauti marinai. Il nuovo JoJo, Jotaro Kujo, nipote giapponese dell\'ultrasessantenne Joseph Joestar, scopre di essere un portatore di \"stand\", un\'estensione del corpo e della mente del suo portatore, generata dalla sua stessa energia vitale. Questo potere è risvegliato in tutti i discendenti diretti della famiglia Joestar dai \"segnali di pericolo\" inviati dal corpo di Jonathan Joestar, di cui Dio Brando si è impadronito, dopo che questi è riuscito a sviluppare in qualche modo il suo potere. Lo stand, i cui poteri differiscono da persona a persona, viene controllato con successo da Jotaro e Joseph, ma rischia di uccidere Holly Joestar, la madre di Jotaro e figlia di Joseph, poiché priva della fortezza d\'animo necessaria a controllarlo: l\'unico rimedio per evitare la morte e riportare lo stand a uno stadio dormiente è uccidere la fonte del male, Dio Brando, che dimora in un antico palazzo de Il Cairo, in Egitto. Inizia così un avventuroso viaggio dal Giappone fino all\'Egitto attraverso l\'Asia, nel quale Jotaro, Joseph e il loro gruppo di alleati portatori di stand, ossia lo studente giapponese Noriaki Kakyoin, l\'indovino egiziano Mohammed Abdul, lo spadaccino francese Jean Pierre Polnareff e il Boston Terrier Iggy, affrontano numerosi portatori di stand nemici, i cui nomi degli stand derivano dagli Arcani maggiori dei Tarocchi e dalle divinità egizie. Lo scontro finale con Dio e il suo The World, in grado di fermare il tempo, vede vittorioso Jotaro e il suo Star Platinum, che vendicano la morte dei loro alleati Abdul, Iggy e Kakyoin disintegrando il corpo del nemico e spezzando la maledizione che grava sulla madre.', 9, '51bhdaN6iXL._SX336_BO1,204,203,200_.jpg', 8, 1, 'Hirohiko Araki', 1992),
(62, 'Jojo\'s Bizzarre Adventure - Vento Aureo', '2001, Italia. Giorno Giovanna, portatore dello stand Gold Experience e figlio che Dio Brando ha concepito usando il corpo di Jonathan Joestar, ha ereditato i valori e lo spirito dei Joestar, e aspira a diventare il più importante boss mafioso, o una \"gangStar\", come dice lui, per controllare la criminalità della città di Napoli e porre fine alle molte ingiustizie che coinvolgono innocenti, come il traffico di droga. Diventa a tale scopo un membro di \"Passione\", un\'organizzazione mafiosa molti dei cui esponenti possiedono uno stand, entrando in una squadra composta dal pistolero Guido Mista, il giovane e ingenuo Narancia Ghirga, l\'ex poliziotto Leone Abbacchio, il razionale e irascibile Fugo Pannacotta e il capo Bruno Bucciarati, che confida nella forza del giovane. In un primo momento Giorno e il suo gruppo obbediscono agli ordini del Boss con il compito di proteggere sua figlia, Trish Una, dai membri della Squadra Esecuzioni, che vorrebbero sfruttarla per scoprire la vera identità del misterioso e sfuggente padre. In seguito, però, scoprono che questi in realtà vuole ucciderla proprio perché ultimo elemento che permetta di rintracciarlo e tagliarsi quindi fuori dal resto del mondo; il suo stand King Crimson è l\'apoteosi di questo desiderio, poiché gli permette di farla sempre franca cancellando dalla percezione altrui qualunque azione voglia compiere e sfuggire al fato. Giorno, Trish e i loro compagni devono così scappare dal boss e dall\'Unità Speciale, una squadra di sicari rimasta fedele al boss pronta a tutto pur di ucciderli. Aiutati da un personaggio proveniente dal passato, il francese Jean-Pierre Polnareff, si viene a sapere di più sulle Frecce della serie precedente: queste furono create anni addietro dal metallo proveniente da un meteorite abitato da un virus alieno in grado di uccidere istantaneamente o donare poteri a chi viene colpito, e si scopre che Jotaro e Polnareff erano consapevoli della loro esistenza (ma non dei loro poteri) e ne erano alla ricerca da anni. Quest\'ultimo offre il proprio aiuto al gruppo rivelando loro che, colpendo con la freccia il proprio stand, se ne ottiene una versione potenziata dal nome di Requiem. Diavolo emerge finalmente dall\'oscurità che si è creato attorno per possedere il potere di Requiem, ma la freccia sceglie Giorno, che annienta il suo nemico spedendolo in un limbo di morti infinite con il suo Gold Experience Requiem, e diventa così il nuovo boss di Passione.', 8, '51Co2DMS5oL._SX327_BO1,204,203,200_.jpg', 4, 1, 'Hirohiko Araki', 1999);

-- --------------------------------------------------------

--
-- Struttura della tabella `cart`
--

CREATE TABLE `cart` (
  `orderid` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `articleid` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `quantità` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struttura della tabella `notifiche`
--

CREATE TABLE `notifiche` (
  `notifyid` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `sellerid` int(11) NOT NULL,
  `articleid` int(11) NOT NULL,
  `quantitya` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `notifiedC` int(11) NOT NULL,
  `notifiedS` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `notifiche`
--

INSERT INTO `notifiche` (`notifyid`, `userid`, `sellerid`, `articleid`, `quantitya`, `type`, `notifiedC`, `notifiedS`) VALUES
(227, 20, 21, 53, 7, 0, 1, 1),
(228, 20, 21, 53, 7, 1, 1, 1),
(229, 20, 21, 53, 7, 2, 1, 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `ordini`
--

CREATE TABLE `ordini` (
  `orderid` int(11) NOT NULL,
  `clientid` int(11) NOT NULL,
  `articleid` int(11) NOT NULL,
  `quant` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `ordini`
--

INSERT INTO `ordini` (`orderid`, `clientid`, `articleid`, `quant`, `status`) VALUES
(169, 20, 53, 7, 2);

-- --------------------------------------------------------

--
-- Struttura della tabella `userarticles`
--

CREATE TABLE `userarticles` (
  `userid` int(11) DEFAULT NULL,
  `productid` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `userarticles`
--

INSERT INTO `userarticles` (`userid`, `productid`) VALUES
(5, 14),
(21, 44),
(21, 45),
(21, 46),
(21, 47),
(21, 48),
(21, 49),
(21, 50),
(21, 51),
(21, 52),
(21, 53),
(21, 54),
(21, 55),
(21, 56),
(21, 57),
(21, 58),
(21, 59),
(21, 60),
(21, 61),
(21, 62);

-- --------------------------------------------------------

--
-- Struttura della tabella `utenti`
--

CREATE TABLE `utenti` (
  `idutente` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(512) NOT NULL,
  `nome` varchar(45) NOT NULL,
  `tipoutente` text DEFAULT 'cliente',
  `saldo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `utenti`
--

INSERT INTO `utenti` (`idutente`, `username`, `password`, `nome`, `tipoutente`, `saldo`) VALUES
(5, 'nicola.bartolucci2@studio.unibo.it', '$2y$10$JD56YHxNDWeXHaN8b6yY6eR33bIuxBHC1Fmmx5s.3mHkT6FDv5URa', 'Nicola Bartolucci', 'admin', 11),
(20, 'utente@gmail.com', '$2y$10$VgJtb3d3eTlYEKknsFxszedRHOlHuxxa7OZ/r5EEVdYrXbCogb10a', 'Utente', 'cliente', 0),
(21, 'davide.barbaro2@studio.unibo.it', '$2y$10$hK.zX4OQn0ec.akPmXdUrel7ARfiVohohJhhNvNVDCbfmUHsud6Fy', 'Davide', 'admin', 0),
(22, 'edoardo.montanari8@studio.unibo.it', '$2y$10$sr5goEpwQXkKrUJhlWJLdONmqidVNiUORsFINbUbxViJll4PbNFp.', 'Edoardo', 'admin', 0);

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `articolo`
--
ALTER TABLE `articolo`
  ADD PRIMARY KEY (`idarticolo`);

--
-- Indici per le tabelle `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`orderid`),
  ADD KEY `userid` (`userid`),
  ADD KEY `articleid` (`articleid`);

--
-- Indici per le tabelle `notifiche`
--
ALTER TABLE `notifiche`
  ADD PRIMARY KEY (`notifyid`);

--
-- Indici per le tabelle `ordini`
--
ALTER TABLE `ordini`
  ADD PRIMARY KEY (`orderid`) USING BTREE;

--
-- Indici per le tabelle `userarticles`
--
ALTER TABLE `userarticles`
  ADD KEY `userid` (`userid`),
  ADD KEY `productid` (`productid`);

--
-- Indici per le tabelle `utenti`
--
ALTER TABLE `utenti`
  ADD PRIMARY KEY (`idutente`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `articolo`
--
ALTER TABLE `articolo`
  MODIFY `idarticolo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT per la tabella `cart`
--
ALTER TABLE `cart`
  MODIFY `orderid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=218;

--
-- AUTO_INCREMENT per la tabella `notifiche`
--
ALTER TABLE `notifiche`
  MODIFY `notifyid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=230;

--
-- AUTO_INCREMENT per la tabella `ordini`
--
ALTER TABLE `ordini`
  MODIFY `orderid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=170;

--
-- AUTO_INCREMENT per la tabella `utenti`
--
ALTER TABLE `utenti`
  MODIFY `idutente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `cart`
--
ALTER TABLE `cart`
  ADD CONSTRAINT `cart_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `utenti` (`idutente`),
  ADD CONSTRAINT `cart_ibfk_2` FOREIGN KEY (`articleid`) REFERENCES `articolo` (`idarticolo`);

--
-- Limiti per la tabella `userarticles`
--
ALTER TABLE `userarticles`
  ADD CONSTRAINT `userarticles_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `utenti` (`idutente`),
  ADD CONSTRAINT `userarticles_ibfk_2` FOREIGN KEY (`productid`) REFERENCES `articolo` (`idarticolo`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
