<!DOCTYPE html>
<html lang="it">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Inserimento Prodotto</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="./css/style.css" />
  <script src="js/jquery-3.4.1.min.js"></script>
  <script src="js/functions.js"></script>
</head>

<body>
  <main>
    <div class="container-fluid">

      <div class="row">
        <div class="col-12 mb-5 px-0">
          <header class="py-2 text-white bg-dark mx-auto">
            <div class="text-center">
              <a class="link-unstyled" href="index.php">
                <img src="css/logo-2.png" alt="logo" class="img-fluid">
              </a>
            </div>
          </header>
        </div>
      </div>

      <div class="row mt-4 mb-3">

      </div>

      <?php if($_POST["mode"]==1): ?>
        <div class="col-12 col-sm-12 col-md-8 col-lg-8 mx-auto">
            <form class="border mt-4 mb-4" action="invia-articolo.php" method="POST" enctype="multipart/form-data">
              <h2 class="text-center mt-2 mb-4">Inserimento Articolo</h2>
              <!--Uso una ROW per mettere certe input box sulla stessa riga con schermi larghi-->
              <div class="row">
                <!--Primo gruppo, a sinistra-->
                <div class="col-12 col-md-12 col-lg-6">
                  <div class="form-group row mt-4 mb-4 px-4 mx-auto">
                    <label class="col-12" for="nomeprodotto">Nome del prodotto</label>
                    <input type="text" class="col-12" id="nomeprodotto" name="Nome" title="Nome del prodotto" required/>
                  </div>

                  <div class="form-group row mt-4 mb-4 px-4 mx-auto">
                    <label class="col-12" for="descrizione">Descrizione</label>
                    <textarea class="col-12" id="descrizione" name="Desc" rows="4" cols="50" required></textarea>
                  </div>

                  <div class="form-group row mt-4 mb-0 px-4 mx-auto">
                    <label class="col-12" for="autore">Autore</label>
                    <input type="text" class="col-12" id="autore" name="Autore" title="Autore dell'opera" required/>
                  </div>
                </div>
                <!--Secondo gruppo, a destra-->
                <div class="col-12 col-md-12 col-lg-6">
                  <div class="form-group row mt-4 mb-0 px-4 mx-auto justify-content-between">
                    <label class="col-3" for="anno">Anno</label>
                    <label class="col-3" for="quantity">Quantità</label>
                    <label class="col-3" for="prezzo">Prezzo (€)</label>
                  </div>

                  <div class="form-group row mt-0 mb-4 px-4 mx-auto justify-content-between">
                    <input class="col-3" id="anno" type="number" name="Anno" value="" min="1950" max="2022" onkeypress="return onlyNumberKey(event)" required>
                    <input class="col-3" id="quantity" type="number" name="Quantity" min="1" value="" required>
                    <input class="col-3" id="prezzo" type="number" name="Prezzo" min="1" value="" required>
                  </div>
                </div>
              </div>
              <div class="form-group row mt-4 mb-4 px-4 mx-auto">
                <label class="col-12" for="imgarticolo">Img</label>
                <input type="file" name="imgarticolo" id="imgarticolo" required/>
              </div>


               <!--<div class="col-md-4">
                <div class="img-square-wrapper">
                  <label class="mx-auto"for="imgarticolo">Img </label>
                  <input type="file" name="imgarticolo" id="imgarticolo" required/>
                </div>
               </div>-->

             <input type="hidden" name="mode" value="<?php echo $_POST["mode"] ?>">
               <div class="row mt-3 mb-3 justify-content-center">
                 <input class="btn btn-primary" id="submit" type="submit" name="pulsanteconferma" value="Conferma">
               </div>
         </form>

       </div>

      <?php endif; ?>

      <!-- se il valore è == 2 allora elimina l'articolo -->

     <?php if($_POST["mode"]==2): ?>
       <div class="d-flex justify-content-center">
         <div class="custom-box bg-white border mt-4 mb-4">
           <h2> Sei sicuro? </h2>
           <div class="row mt-3 mb-3 d-flex justify-content-center">
             <form action="invia-articolo.php" method="post">
               <input type="hidden" name="mode" value="<?php echo $_POST["mode"] ?>">
               <input type="hidden" name="ids" value="<?php echo $_POST["ids"] ?>">
               <input class="btn btn-primary" type="submit" name="" value="Si">
             </form>
           </div>
           <div class="row mt-3 mb-3 d-flex justify-content-center">
             <form action="elenco.php" method="post">
               <input class="btn btn-primary" type="submit" name="" value="No">
             </form>
           </div>
         </div>
       </div>

     <?php endif;  ?>
</div>
</main>
</body>
</html>
