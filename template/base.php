<!DOCTYPE html>
<html lang="it">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?php echo $templateParams["titolo"]; ?></title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/css/toastr.css" rel="stylesheet"/>
  <link rel="stylesheet" type="text/css" href="./css/style.css" />
  <script src="js/jquery-3.4.1.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/js/toastr.js"></script>
  <script src="js/functions.js"></script>
  <script src="js/notifiche.js"></script>
</head>
<body>
  <main>
    <div class="container-fluid">

      <!--Header del sito-->
      <div class="row">
        <div class="col-12 mb-5 px-0">
          <header class="px-1 py-0 bg-dark mx-auto">
            <div class="col-12 text-center">
              <div class="row">
                <!--Logo del sito-->
                <div class="col-md-4 my-auto">
                  <a class="link-unstyled" href="index.php">
                    <img src="css/logo-2.png" alt="logo" class="img-fluid">
                  </a>
                </div>
                <!--Barra di ricerca-->
                <div class="col-md-4 my-auto py-1 custom-search">
                  <div class="custom-search">
                   <form class="bg-dark" action="index.php" method="GET">
                     <input type="text" class="custom-search-input" placeholder="Ricerca..." title="Ricerca" name="ricerca">
                     <button class="custom-search-botton" type="submit"><span class="fa fa-search"></span></button>
                   </form>
                 </div>
                </div>
                <!--Altri pulsanti (account, carrello, notifiche)-->
                <div class="col-md-4 my-auto py-1">
                  <div class="row">
                    <div class="col-4">
                      <a class="btn" href="login.php"><span class="sr-only">Login</span><span class="fa fa-user fa-2x"></span></a>
                    </div>
                    <div class="col-4">
                      <a class="btn" href="notifiche.php"><span class="sr-only">Notifiche</span><span id="bell" class="fa fa-bell fa-2x"></span></a>
                    </div>
                    <div class="col-4">
                      <form class="bg-dark cart" action="carrello.php" method="post">
                        <input type="hidden" name="operation" value="2">
                        <button class="btn" type="submit"><span class="fa fa-shopping-cart fa-2x"></span></button>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </header>
        </div>
      </div>

      <!--Contenuto-->
      <div class="contenuto">
        <?php
        if(isset($templateParams["nome"])){
            require($templateParams["nome"]);
        }
        ?>
      </div>

      <div id="tost" class="toast-container hide">
        <div class="toast mt-3 hide" data-autohide="false">
          <div class="toast-header">
            <strong class="mr-auto text-primary">Nuove notifiche</strong>
            <button id="tostbtn" type="button" class="ml-2 mb-1 close" data-dismiss="toast">&times;</button>
          </div>
          <div class="toast-body">
            <p id="tosttext">Nuova notifica</p>
          </div>
        </div>
      </div>

          <!--Footer del sito-->
      <div class="row">
        <div class="col-12 mt-5 px-0">
          <div class="footer mx-auto px-1 py-2 text-white bg-dark">
            <p class="h4 text-center font-italic">Non Ci Manga Niente - Contatti</p>
            <div class="col-12 text-center">  <!--Necessario per evitare un padding eccessivo delle singole col-->
              <div class="row">
                <div class="col">
                  <p class="h6 text-center font-italic">nicola.bartolucci2@studio.unibo.it</p>
                </div>
                <div class="col">
                  <p class="h6 text-center font-italic">davide.barbaro2@studio.unibo.it</p>
                </div>
                <div class="col">
                  <p class="h6 text-center font-italic">edoardo.montanari8@studio.unibo.it</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </main>
</body>
</html>
