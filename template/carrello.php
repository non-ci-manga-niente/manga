    <?php if($usercart!=NULL):
      $i=0;?>
    <?php foreach ($usercart as $articoli):
      $i++;
      $idpulsante="quantity";
      $idpulsante = "{$idpulsante}{$i}";?>
        <div class="custom-box border mt-4 mb-4">
        <div class="card my-2 mx-2">
          <div class="row no-gutters">
            <div class="col-md-4">
              <div class="thumbnail-box border my-2 mx-auto">
                <img class="d-block mx-auto product-image" src="<?php echo UPLOAD_DIR.$articoli["img"]; ?>" alt="Immagine prodotto">
              </div>
            </div>
            <div class="col-md-8">
              <div class="card-body">
                <div class="row">
                  <div class="col-12 col-md-12 col-lg-8 mb-4">
                    <h4 class="card-title"><?php echo $articoli["nome"] ?></h4>
                    <p class="card-text mb-1"><?php echo $articoli["descr"] ?></p>
                    <p class="card-text mb-1">Prezzo: <?php echo $articoli["prezzo"]; ?> €</p>
                    <p class="card-text mb-1">Pezzi rimasti: <?php echo $articoli["quantità"] ?></p>
                  </div>
                  <div class="col-12 col-md-12 col-lg-4 text-center my-auto">
                    <form action="invia-articolo.php" method="POST">
                      <div class="row">
                        <div class="col-6 my-auto">
                          <label for="<?php echo $idpulsante; ?>" class="sr-only">Quantità</label>
                          <input type="number" id="<?php echo $idpulsante; ?>" name="quantity" min="1" max ="<?php echo $articoli["quantità"]?>" value="1">
                        </div>
                        <input type="hidden" name="mode" value="3">
                        <input type="hidden" name="ids" value="<?php echo $articoli["orderid"]; ?>">
                        <input type="hidden" name="userid" value="<?php echo $userid ?>">
                        <div class="col-6">
                          <input type="submit" name="rimuovi" value="Rimuovi" class="btn btn-primary btn-lg">
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    <?php $totale += $articoli["prezzo"]*$articoli["quantità"];  ?>
    <?php endforeach; ?>
  <?php endif; ?>
  <!--Parte in basso, con il costo complessivo e pulsanti per procedere-->
  <div class="row mx-1">
    <div class="col-sm-4 col-md-6 col-lg-6">
      <h2>Costo totale: <?php echo $totale; ?> €</h2>
    </div>
    <div class="col-sm-8 col-md-6 col-lg-6">
      <div class="row">
        <div class="col-6 text-center">
          <form class="bg-white" action="checkout.php" method="POST">
            <input type="hidden" name="mode" value="4">
            <input type="hidden" name="totale" value="<?php echo $totale; ?>">
            <button class="btn btn-primary btn-lg" type="submit" name="button">Procedi con l'acquisto!</button>
          </form>
        </div>
        <div class="col-6 text-center my-auto">
          <a class="btn btn-success btn-lg" href="index.php">Torna a comprare</a>
        </div>
      </div>
    </div>


  </div>
