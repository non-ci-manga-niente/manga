<!DOCTYPE html>
<html lang="it">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Pagina di Login</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="./css/style.css" />
</head>

<body>
  <main>
    <div class="container-fluid">

      <div class="row">
        <div class="col-12 mb-5 px-0">
          <header class="py-2 text-white bg-dark mx-auto">
            <div class="text-center">
              <a class="link-unstyled" href="index.php">
                <img src="css/logo-2.png" alt="logo" class="img-fluid">
              </a>
            </div>
          </header>
        </div>
      </div>

      <div class="row mt-4 mb-3">

      </div>

      <div class="row">
        <!--Un alternativa all'uso di mx-auto è quella di creare due div vuoti col-md-3-->
        <div class="col-12 col-sm-8 col-md-6 col-lg-4 mt-5 mx-auto">
          <form class="border mt-4 mb-4" action="#" method="POST">
            <h2 class="text-center mt-2">Login</h2>
            <?php if(isset($templateParams["login-status"])): ?>
              <p class="text-center"><?php echo $templateParams["login-status"]; ?></p>
            <?php endif; ?>
            <div class="form-group row mt-4 mb-4">
              <input type="text" class="col-10 mx-auto" placeholder="Email" id="username" name="username" title="Inserisci l'indirizzo email" required/>
            </div>
            <div class="form-group row mt-4 mb-4">
              <input type="password" class="col-10 mx-auto" placeholder="Password" id="password" name="password" title="Inserisci la password" required/>
            </div>
            <div class="form-group row mt-4 mb-4">
              <button type="submit" class="btn btn-primary col-10 mx-auto p-2">Accedi</button>
            </div>
            <div class="container text-center mb-3">
              <span class="font-weight-light">Non hai un account? <a href="signup.php">Registrati qui</a></span>
            </div>
          </form>
        </div>
      </div>
    </div>
  </main>
</body>
</html>
