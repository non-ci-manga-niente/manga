<!DOCTYPE html>
<html lang="it">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?php echo $templateParams["titolo"]; ?></title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="./css/style.css" />
</head>

<body>
  <main>
    <div class="container-fluid">
      <div class="row">

        <!--Header del sito-->
        <div class="col-12 mb-5 px-0">
          <header class="px-1 py-0 bg-dark mx-auto">
            <div class="col-12 text-center">
              <div class="row">
                <!--Logo del sito-->
                <div class="col-md-4 my-auto">
                  <a class="link-unstyled" href="index.php">
                    <img src="css/logo-2.png" alt="logo" class="img-fluid">
                  </a>
                </div>
                <!--Barra di ricerca-->
                <div class="col-md-4 my-auto py-2 custom-search">
                  <div class="custom-search">
                    <input type="text" class="custom-search-input" placeholder="Ricerca..." title="Ricerca">
                    <button class="custom-search-botton" type="submit"><span class="fa fa-search"></span></button>
                  </div>
                </div>
                <!--Altri pulsanti (account, carrello, notifiche, burger)-->
                <div class="col-md-4 my-auto py-2">
                  <div class="row">
                    <div class="col-3">
                      <a class="btn" href="login.php"><span class="fa fa-user fa-2x"></span></a>
                    </div>
                    <div class="col-3">
                      <a class="btn" href="#"><span class="fa fa-bell fa-2x"></span></a>
                    </div>
                    <div class="col-3">
                      <a class="btn" href="#"><span class="fa fa-shopping-cart fa-2x"></span></a>
                    </div>
                    <div class="col-3">
                      <a class="btn" href="#"><span class="fa fa-bars fa-2x"></span></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </header>
        </div>
      </div>

    <!--Profilo utente-->
    <div class="row">
      <!--Un alternativa all'uso di mx-auto è quella di creare due div vuoti col-md-3-->
      <!--Qui prima c'era anche mt-5-->
      <div class="col-12 col-sm-8 col-md-6 col-lg-4 mx-auto">
        <div class="custom-box bg-white border mt-4 mb-4">
          <h2 class="mt-2 mx-2">Ciao, <?php echo $_SESSION["Utente"]; ?></h2>
          <p class="mb-1 mx-2">Qui potrai gestire il tuo account</p>
          <p class="mb-1 mx-3">Tipo account: <?php echo $_SESSION["Type"]; ?></p>
          <div class="row mt-4 mb-3">
            <a href="#" class="btn btn-primary col-10 mx-auto p-2">Modifica listino</a>
          </div>
          <div class="row mt-3 mb-3">
            <a href="#" class="btn btn-primary col-10 mx-auto p-2">Notifiche</a>
          </div>
          <div class="row mt-3 mb-3">
            <a href="#" class="btn btn-primary col-10 mx-auto p-2">Visualizza ordini</a>
          </div>
          <div class="row mt-3 mb-4">
            <a href="logout.php" class="btn btn-primary col-10 mx-auto p-2">Esci</a>
          </div>
        </div>
      </div>
    </div>

    <!--Footer del sito-->
    <div class="row">
      <div class="col-12 mt-5 px-0">
        <div class="footer mx-auto py-2 text-white bg-dark">
          <!--<footer class="fixed-bottom py-2 text-white bg-dark">-->
          <p class="h4 text-center font-italic">Non Ci Manga Niente - Contatti</p>
          <div class="row">
            <div class="col">
              <p class="h6 text-center font-italic">nicola.bartolucci2@studio.unibo.it</p>
            </div>
            <div class="col">
              <p class="h6 text-center font-italic">davide.barbaro2@studio.unibo.it</p>
            </div>
            <div class="col">
              <p class="h6 text-center font-italic">edoardo.montanari8@studio.unibo.it</p>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
  </main>
</body>
</html>
