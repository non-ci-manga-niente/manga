      <?php if(empty($templateParams["notifica"])): ?>
        <div class="col-12 col-sm-8 col-md-6 col-lg-4 mx-auto">
          <div class="custom-box bg-white border mt-4 mb-4">
            <h2 class="text-center">Nessuna notifica!</h2>
          </div>
        </div>
      <?php endif;
      if(!empty($templateParams["notifica"])): ?>
      <div class="row justify-content-center">

      <?php
       foreach ($templateParams["notifica"] as $articoli):
         if($articoli["type"] != 1 && $articoli["userid"] == $_SESSION["id"]): ?>

          <div class="custom-box border my-4 mx-4 px-0 col-10 col-sm-6 col-md-4 col-lg-3">
            <div class="card">
              <div class="thumbnail-box border my-2 mx-auto">
                <img class="d-block mx-auto product-image card-img-top" src="<?php echo UPLOAD_DIR.$articoli["img"]; ?>" alt="Immagine prodotto">
              </div>
              <div class="card-body">
                      <h3 class="card-title mb-0"><?php echo $articoli["nome"] ?></h3>
                      <p class="card-text mb-0">Pezzi acquistati: <?php echo $articoli["quantitya"] ?></p>
                      <p class="card-text">Prezzo totale: <?php echo $articoli["prezzo"]*$articoli["quantitya"] ?> €</p>

                      <?php if($articoli["type"] == 0): //0 = Prodotto acquistato. 2 = Prodotto spedito. 3 = Prodotto disdetto. 4 = Prodotto arrivato. 1 = Scorte finite. ?>
                            <h3 class="card-title">Hai acquistato questo prodotto!</h3>
                      <?php endif; ?>
                      <?php if($articoli["type"] == 2): ?>
                            <h3 class="card-title">Il prodotto è stato spedito!</h3>
                      <?php endif; ?>
                      <?php if($articoli["type"] == 3): ?>
                            <h3 class="card-title">Hai disdetto questo acquisto!</h3>
                      <?php endif; ?>
                      <?php if($articoli["type"] == 4): ?>
                            <h3 class="card-title">Il prodotto è arrivato!</h3>
                      <?php endif; ?>

              </div>

            </div>
          </div>

      <?php elseif($_SESSION["Type"] == "admin"): ?>

        <div class="custom-box border my-4 mx-4 px-0 col-10 col-sm-6 col-md-4 col-lg-3">
           <div class="card">
               <div class="thumbnail-box border my-2 mx-auto">
                 <img class="d-block mx-auto product-image card-img-top" src="<?php echo UPLOAD_DIR.$articoli["img"]; ?>" alt="Immagine prodotto">
               </div>
               <div class="card-body">

                     <h3 class="card-title mb-0"><?php echo $articoli["nome"] ?></h3>
                     <?php if($articoli["type"] != 1): ?>
                     <p class="card-text mb-0">Pezzi acquistati: <?php echo $articoli["quantitya"] ?></p>
                     <p class="card-text">Prezzo totale: <?php echo $articoli["prezzo"]*$articoli["quantitya"] ?> €</p>
                     <?php endif; ?>

                     <?php if($articoli["type"] == 0): ?>
                         <h3 class="card-title">L'utente <?php echo $articoli["username"] ?> ha comprato il prodotto!</h3>
                     <?php endif; ?>
                     <?php if($articoli["type"] == 1): ?>
                           <h3 class="card-title">Scorte esaurite!</h3>
                     <?php endif; ?>
                     <?php if($articoli["type"] == 2): ?>
                           <h3 class="card-title">Hai spedito questo prodotto!</h3>
                     <?php endif; ?>
                     <?php if($articoli["type"] == 3): ?>
                           <h3 class="card-title">L'acquisto è stato disdetto!</h3>
                     <?php endif; ?>
                     <?php if($articoli["type"] == 4): ?>
                           <h3 class="card-title">Il prodotto è arrivato a destinazione!</h3>
                     <?php endif; ?>

             </div>
           </div>
         </div>
     <?php endif; endforeach; endif; ?>
</div>
