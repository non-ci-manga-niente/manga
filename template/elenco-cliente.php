    <?php if (isset($_COOKIE["found"])): ?>
      <div class="col-12 col-sm-8 col-md-6 col-lg-4 mx-auto">
        <div class="custom-box bg-white border mt-4 mb-4">
          <h2 class="text-center"><?php echo $empty;?></h2>
        </div>
      </div>
    <?php  endif; ?>


    <?php if(empty($templateParams["narticoli"])): ?>
      <div class="col-12 col-sm-8 col-md-6 col-lg-4 mx-auto">
        <div class="custom-box bg-white border mt-4 mb-4">
          <h2 class="text-center">La ricerca non ha portato risultati</h2>
        </div>
      </div>
    <?php endif;
    if(!empty($templateParams["narticoli"])):
      $i=0;
      foreach ($templateParams["narticoli"] as $articoli):
          $i++;
          $idpulsante="quantity";
          $idpulsante = "{$idpulsante}{$i}";?>
          <div class="custom-box border mt-4 mb-4">
            <div class="card my-2 mx-2">
              <div class="row no-gutters">
                <!--Parte con l'immagine, dovrebbe scorrere in alto in mobile-->
                <div class="col-md-4">
                  <div class="thumbnail-box border my-2 mx-auto ">
                    <a href ="product.php?id=<?php echo $articoli["idarticolo"]; ?>"><img class="d-block mx-auto product-image" src="<?php echo UPLOAD_DIR.$articoli["img"]; ?>" alt="Immagine prodotto"></a>
                  </div>
                </div>
                <!--Parte con testo e pulsanti-->
                <div class="col-md-8">
                    <div class="card-body">
                      <div class="row">
                        <div class="col-12 col-md-12 col-lg-8 mb-4">
                          <a class="link-unstyled" href ="product.php?id=<?php echo $articoli["idarticolo"]; ?>"><h4 class="card-title"><?php echo $articoli["nome"] ?></h4></a>
                          <p class="card-text mb-1"><?php if(strlen($articoli["descr"]) <= 200){
                                                            echo $articoli["descr"];
                                                          }
                                                          else{
                                                            echo ("" . substr($articoli["descr"], 0, 200) . "...");
                                                          }  ?></p>
                          <p class="card-text mb-1">Prezzo: <?php echo $articoli["prezzo"] ?> €</p>
                          <p class="card-text mb-1">Pezzi rimasti: <?php echo $articoli["quantity"] ?></p>
                        </div>
                        <div class="col-12 col-md-12 col-lg-4 text-center my-auto">
                          <?php if(($articoli["quantity"]!=0) && ($articoli["userid"]) != $_SESSION["id"]): ?>
                            <form action="carrello.php" method="POST" class="">
                              <div class="row">
                                <div class="col-6 my-auto">
                                  <label for="<?php echo $idpulsante; ?>" class="sr-only">Quantità</label>
                                  <input type="number" id="<?php echo $idpulsante; ?>" name="quantity" min="1" max ="<?php echo $articoli["quantity"]?>" value="1">
                                </div>
                                <input type="hidden" name="operation" value="1">
                                <input type="hidden" name="ids" value="<?php echo $articoli["idarticolo"]; ?>">
                                <div class="col-6">
                                  <input type="submit" name="pulsantecompra" value="COMPRA" class="btn btn-primary btn-lg">
                                </div>
                              </div>

                            </form>
                          <?php else:
                             if($articoli["userid"] != $_SESSION["id"]): ?>
                          <button class="btn btn-secondary btn-lg" type="button" name="status"> Status: All Sold OUT </button>
                          <?php else: ?>
                          <button class="btn btn-secondary btn-lg" type="button" name="status"> COMPRA </button>
                        <?php endif; endif; ?>
                          <!-- <button type="button" name="button">Elimina</button> -->
                        </div>
                      </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
  <?php endforeach; endif;?>
