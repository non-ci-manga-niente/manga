    <!--Profilo utente-->
    <div class="row">
      <!--Un alternativa all'uso di mx-auto è quella di creare due div vuoti col-md-3-->
      <!--Qui prima c'era anche mt-5-->
      <div class="col-12 col-sm-8 col-md-6 col-lg-4 mx-auto">
        <div class="custom-box border mt-4 mb-4">
          <h2 class="mt-2 mx-2">Ciao, <?php echo $_SESSION["Utente"]; ?></h2>
          <p class="mb-1 mx-2">Qui potrai gestire il tuo account</p>
          <div class="row">
            <div class="col-6">
              <p class="mb-1 mx-3">Tipo account: <?php echo $_SESSION["Type"]; ?></p>
            </div>
          </div>
          <?php if ($_SESSION["Type"]=="admin"): ?>
            <div class="row mt-4 mb-3">
              <a href="elenco.php" class="btn btn-primary col-10 mx-auto p-2">Modifica listino</a>
            </div>
          <?php endif; ?>
          <div class="row mt-3 mb-3">
            <a href="notifiche.php" class="btn btn-primary col-10 mx-auto p-2">Notifiche</a>
          </div>
          <div class="row mt-3 mb-3">
            <a href="orders.php" class="btn btn-primary col-10 mx-auto p-2">Visualizza ordini</a>
          </div>
          <div class="row mt-3 mb-4">
            <a href="logout.php" class="btn btn-primary col-10 mx-auto p-2">Esci</a>
          </div>
        </div>
      </div>
    </div>
