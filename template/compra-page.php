      <?php if($errore == 1): ?>
        <div class="col-12 col-sm-8 col-md-6 col-lg-4 mx-auto">
          <div class="custom-box border mt-4 mb-4">
            <h2 class="text-center">Qualcosa è andato storto nell'acquisto!</h2>
          </div>
        </div>
      <?php endif; ?>
      <?php if($errore == 0): ?>
        <div class="col-12 col-sm-8 col-md-6 col-lg-4 mx-auto">
          <div class="custom-box border mt-4 mb-4">
            <h2 class="text-center">Aquisto effettuato con successo!</h2>
          </div>
        </div>
      <?php endif; ?>
