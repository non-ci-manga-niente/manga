

      <!-- <button type="button" name="button"><a href="form-prodotto.php">aggiungi articoli</a></button> -->
      <?php if (isset($_COOKIE["error"])): ?>
        <div class="col-12 col-sm-8 col-md-6 col-lg-4 mx-auto">
          <div class="custom-box bg-white border mt-4 mb-4">
            <h2 class="text-center"><?php echo $_COOKIE["error"];?></h2>
          </div>
        </div>
      <?php  endif; ?>

      <?php if (isset($_COOKIE["aumento"])): ?>
        <div class="col-12 col-sm-8 col-md-6 col-lg-4 mx-auto">
          <div class="custom-box bg-white border mt-4 mb-4">
            <h2 class="text-center"><?php echo $_COOKIE["aumento"];?></h2>
          </div>
        </div>
      <?php  endif; ?>

      <div class="col-12 col-sm-8 col-md-6 col-lg-4 mx-auto">

          <!--Nel caso in cui non ci siano ancora articoli-->
          <?php if(empty($templateParams["articoli"])): ?>
            <h2 class="text-center">Sembra che non hai articoli, comincia a vendere!</h2>
          <?php endif; ?>
          <form class="bg-white no-border" action="form-prodotto.php" method="POST">
            <input type="hidden" name="mode" value="1">
            <div class="row mt-3 mb-5">
              <input type="submit" class="btn btn-primary col-10 mx-auto p-2" name="aggiuntaarticolo" value="Aggiungi articoli">
            </div>
          </form>

      </div>
      <?php $i=0; ?>
      <!--Nel caso in cui ci siano articoli-->
      <?php if(!empty($templateParams["articoli"])):
       foreach ($templateParams["articoli"] as $articoli):
         //Queste 3 righe servono per generare un ID univoco per ogni pulsante RICARICA creato nel loop
         $i++;
         $idpulsante="quantity";
         $idpulsante = "{$idpulsante}{$i}";?>
            <div class="custom-box border mt-4 mb-4">
              <div class="card my-2 mx-2">
                <div class="row no-gutters">
                  <!--Parte con l'immagine, dovrebbe scorrere in alto in mobile-->
                  <div class="col-md-4">
                    <div class="thumbnail-box border my-2 mx-auto">
                      <img class="d-block mx-auto product-image" src="<?php echo UPLOAD_DIR.$articoli["img"]; ?>" alt="Immagine prodotto">
                    </div>
                  </div>
                  <!--Parte con testo e pulsanti-->
                  <div class="col-md-8">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-12 col-md-12 col-lg-8 mb-4">
                        <h4 class="card-title"><?php echo $articoli["nome"] ?></h4>
                        <p class="card-text mb-1"><?php if(strlen($articoli["descr"]) <= 200){
                                                          echo $articoli["descr"];
                                                        }
                                                        else{
                                                          echo ("" . substr($articoli["descr"], 0, 200) . "...");
                                                        }  ?></p>
                        <p class="card-text mb-1">Prezzo: <?php echo $articoli["prezzo"] ?> €</p>
                        <p class="card-text mb-1">Pezzi rimasti: <?php echo $articoli["quantity"] ?></p>
                      </div>
                      <div class="col-12 col-md-12 col-lg-4 my-auto">
                        <div class="row">
                          <div class="col-6 text-center">
                            <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#ricarica<?php echo $articoli["idarticolo"]; ?>">Ricarica</button>

                            <!-- Modal -->
                            <div class="modal fade" id="ricarica<?php echo $articoli["idarticolo"]; ?>" role="dialog">
                              <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h4 class="modal-title text-left">Ricarica</h4>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  </div>
                                  <div class="modal-body py-2">
                                    <p class="my-0">Seleziona la quantità da ricaricare.</p>
                                  </div>
                                  <div class="modal-footer">
                                    <form class="bg-white" action="ricarica-prodotto.php" method="POST">
                                      <label for="<?php echo $idpulsante; ?>" class="sr-only">Quantità</label>
                                      <input type="number" id="<?php echo $idpulsante; ?>" name="quantity" min="1" value="1" class="align-middle mx-2">
                                      <input type="hidden" name="ids" value="<?php echo $articoli["idarticolo"]; ?>">
                                      <input type="submit" name="pulsantericarica" value="Ricarica" class="btn btn-primary py-1">
                                    </form>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-6 text-center">
                            <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#elimina<?php echo $articoli["idarticolo"]; ?>">Elimina</button>

                            <!-- Modal -->
                            <div class="modal fade" id="elimina<?php echo $articoli["idarticolo"]; ?>" role="dialog">
                              <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h4 class="modal-title text-left">Elimina</h4>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  </div>
                                  <div class="modal-body py-2">
                                    <p class="my-0">Quest'operazione eliminerà il prodotto "<?php echo $articoli["nome"]; ?>" e ogni cronologia di transazioni ad esso collegata. <br/> Sei sicuro?</p>
                                  </div>
                                  <div class="modal-footer">
                                    <form action="invia-articolo.php" method="POST">
                                      <input type="hidden" name="mode" value="2">
                                      <input type="hidden" name="ids" value="<?php echo $articoli["idarticolo"]; ?>">
                                      <input type="submit" name="pulsanteelimina" class="btn btn-primary py-1" value="Elimina">
                                    </form>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                </div>

                </div>
              </div>
            </div>
      <?php endforeach; endif;?>
