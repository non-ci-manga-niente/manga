      <?php if(empty($templateParams["notifica"])): ?>
        <div class="col-12 col-sm-8 col-md-6 col-lg-4 mx-auto">
          <div class="custom-box border mt-4 mb-4">
            <h2 class="text-center">Nessun ordine in corso!</h2>
          </div>
        </div>
      <?php endif;
      if(!empty($templateParams["notifica"])): ?>
      <div class="row justify-content-center">

      <?php
       foreach ($templateParams["notifica"] as $articoli): ?>
          <div class="custom-box border my-4 mx-4 px-0 col-10 col-sm-6 col-md-4 col-lg-3">
          <div class="card">
            <div class="thumbnail-box border my-2 mx-auto">
                <img class="d-block mx-auto product-image card-img-top" src="<?php echo UPLOAD_DIR.$articoli["img"]; ?>" alt="Immagine prodotto">
              </div>
              <div class="card-body">
                    <h3 class="card-title mb-0"><?php echo $articoli["nome"] ?></h3>
                    <p class="card-text mb-0">Pezzi acquistati: <?php echo $articoli["quant"] ?></p>
                    <p class="card-text mb-0">Prezzo totale: <?php echo $articoli["prezzo"]*$articoli["quant"] ?> €</p>

                    <?php if($_SESSION["Type"]=="admin"):
                      if(($articoli["status"] == 1) && $articoli["clientid"] != $_SESSION["id"]): ?>
                      <h3 class="my-3">Stato della vendita</h3>
                      <form action="update-notifiche.php" method="POST">
                        <input type="hidden" name="operation" value="2">
                        <input type="hidden" name="status" value="2">
                        <input type="hidden" name="img" value="<?php echo $articoli["img"] ?>">
                        <input type="hidden" name="nome" value="<?php echo $articoli["nome"] ?>">
                        <input type="hidden" name="prezzo" value="<?php echo $articoli["prezzo"] ?>">
                        <input type="hidden" name="quantity" value="<?php echo $articoli["quant"] ?>">
                        <input type="hidden" name="order" value="<?php echo $articoli["orderid"]?>">
                        <input type="hidden" name="art" value="<?php echo $articoli["idarticolo"]?>">
                        <input type="hidden" name="client" value="<?php echo $articoli["clientid"]?>">
                        <div class="col text-center">
                          <button class="col-12 btn btn-success btn-lg" type="submit" name="status">Conferma la spedizione</button>
                        </div>
                      </form>
                    <?php endif;
                  if(($articoli["status"] != 1) && $articoli["clientid"] != $_SESSION["id"]): ?>
                    <h2 class="my-3">Stato della vendita</h2>
                    <div class="col text-center">
                      <button class="col-12 btn btn-secondary btn-lg" type="button" name="status">Hai spedito questo prodotto</button>
                    </div>
                  <?php endif; endif; ?>

                <?php if($articoli["clientid"] == $_SESSION["id"]):
                  if($articoli["status"] == 1): ?>
                    <h3 class="my-3">Stato dell'acquisto</h3>
                    <div class="col text-center">
                      <button class="col-12 btn btn-secondary btn-lg mb-3" type="button" name="status">In attesa del Venditore</button>
                    </div>
                    <!--<form action="update-notifiche.php" method="POST">
                      <input type="hidden" name="operation" value="1">
                      <input type="hidden" name="status" value="1">
                      <input type="hidden" name="quantity" value="<?php echo $articoli["quant"] ?>">
                      <input type="hidden" name="order" value="<?php echo $articoli["orderid"]?>">
                      <input type="hidden" name="art" value="<?php echo $articoli["idarticolo"]?>">
                      <div class="col text-center">
                        <input type="submit" class="col-12 btn btn-danger bnt-lg" value="Disdici acquisto" name="annulla_acquisto">
                      </div>
                    </form> -->
                    <div class="col text-center">
                      <button type="button" class="col-12 btn btn-primary btn-lg" data-toggle="modal" data-target="#elimina<?php echo $articoli["orderid"]; ?>">Disdici acquisto</button>
                    </div>
                    <!-- Modal -->
                    <div class="modal fade" id="elimina<?php echo $articoli["orderid"]; ?>" role="dialog">
                      <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <h4 class="modal-title text-left">Elimina</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                          </div>
                          <div class="modal-body py-2">
                            <p class="my-0">Quest'operazione eliminerà la transazione di "<?php echo $articoli["nome"]; ?>" e sarai rimborsato dal venditore. <br/> Sei sicuro di continuare?</p>
                          </div>
                          <div class="modal-footer">
                            <form action="update-notifiche.php" method="POST">
                              <input type="hidden" name="operation" value="1">
                              <input type="hidden" name="status" value="1">
                              <input type="hidden" name="quantity" value="<?php echo $articoli["quant"] ?>">
                              <input type="hidden" name="order" value="<?php echo $articoli["orderid"]?>">
                              <input type="hidden" name="art" value="<?php echo $articoli["idarticolo"]?>">
                              <input type="submit" name="pulsanteelimina" class="btn btn-primary py-1" value="Disdici">
                            </form>
                          </div>
                        </div>
                      </div>
                    </div>
                <?php endif;
                  if($articoli["status"] == 2): ?>
                    <h3 class="my-3">Stato dell'acquisto</h3>
                    <form action="update-notifiche.php" method="POST">
                      <input type="hidden" name="status" value="4">
                      <input type="hidden" name="operation" value="0">
                      <input type="hidden" name="quantity" value="<?php echo $articoli["quant"] ?>">
                      <input type="hidden" name="order" value="<?php echo $articoli["orderid"]?>">
                      <input type="hidden" name="art" value="<?php echo $articoli["idarticolo"]?>">
                      <div class="col text-center">
                        <input type="submit" class="col-12 btn btn-success bnt-lg" value="Conferma l'arrivo del prodotto" name="conferma_acquisto">
                      </div>
                    </form>
                <?php endif; endif; ?>

              </div>
            </div>
        </div>
      <?php endforeach; ?>
      </div>
    <?php endif; ?>
