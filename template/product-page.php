<!--Pagina di un prodotto-->
<div class="row ">
  <div class="col-12 col-sm-12 col-md-10 col-lg-8 mx-auto">
    <div class="product-box border  mb-4">
      <div class="row no-gutters">
        <!--Sezione a sinistra, con l'immagine-->
        <div class="col-md-4 my-auto"> <!--my-auto saves the day-->
          <div class="thumbnail-box border my-2 mx-auto">
            <img src="<?php echo UPLOAD_DIR.$templateParams["articolo"][0]["img"] ?>" class="product-image mx-auto d-block" alt="" />
          </div>
        </div>
        <!--Sezione a destra, con dettagli dell'articolo-->
        <div class="col-md-8 px-0">
          <h2 class="mt-4 mx-2"><?php echo $templateParams["articolo"][0]["nome"] ?></h2>
          <h3 class="mt-2 mx-2">Costo: <?php echo $templateParams["articolo"][0]["prezzo"] ?> €</h3>
          <div class="col-12 mx-auto">
            <div class="row mt-4 mb-4 px-0 px-lg-4 justify-content-between">
              <!--<div class="col px-4">
                <a href="#" class="btn btn-primary col-12 py-2">Aggiungi al carrello</a>
              </div>-->
              <div class="col">
                <?php if($templateParams["articolo"][0]["quantity"]!=0): ?>
                  <form action="carrello.php" method="POST">
                    <div class="row"> <!--Odio dover innestare ROW e COL solo per far funzionare sta roba-->
                      <div class="col-6 col-xl-2 text-center my-auto">
                        <label for="quantity" class="sr-only">Quantità</label>
                        <input type="number" id="quantity" name="quantity" min="1" max ="<?php echo $templateParams["articolo"][0]["quantity"]?>" value="1">
                      </div>
                      <input type="hidden" name="operation" value="1">
                      <input type="hidden" name="ids" value="<?php echo $templateParams["articolo"][0]["idarticolo"]; ?>">
                      <div class="col-6 col-xl-4 text-center">
                        <?php if($flag==0): ?>
                        <input type="submit" name="pulsantecompra" value="COMPRA" class="btn btn-primary py-2">
                        <?php else: ?>
                        <button class="btn btn-secondary btn-lg" type="button" name="status"> COMPRA </button>
                        <?php endif; ?>
                      </div>
                    </div>
                  </form>
                  <p>Pezzi rimasti: <?php echo $templateParams["articolo"][0]["quantity"] ?> </p>
                <?php else: ?>
                  <!--<a href="#" class="btn btn-secondary col-12 py-2">Status: All Sold OUT </a>-->
                  <button class="btn btn-secondary btn-lg" type="button" name="status"> Status: All Sold OUT </button>
                <?php endif; ?>
              </div>
            </div>
          </div>

          <h4 class="mt-2 mb-2 mx-2">Dettagli:</h4>
          <ul>
            <li>Autore: <?php echo $templateParams["articolo"][0]["autore"] ?></li>
            <li>Anno: <?php echo $templateParams["articolo"][0]["anno"] ?></li>
            <li>Descrizione: <?php echo $templateParams["articolo"][0]["descr"] ?></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
