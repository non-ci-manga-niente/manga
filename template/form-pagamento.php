<!--Pagina del pagamento-->
<div class="row">
    <div class="col-12 col-sm-8 col-lg-6 col-xl-4 mx-auto">
      <form class="border mt-4 mb-4" action="#" method="POST">
      <h2 class="mt-2 text-center">Inserisci le informazioni per il pagamento</h2>
      <?php if(isset($templateParams["validitacarta"])): ?>
        <p class="text-center"><?php echo $templateParams["validitacarta"]; ?></p>
      <?php endif; ?>

      <div class="form-group row mt-4 mb-4 px-4 mx-auto">
        <label class="col-12" for="indirizzo">Indirizzo di spedizione</label>
        <input type="text" class="col-12" id="indirizzo" name="indirizzo"
         value="Via Cesare Pavese 50, 47521 Cesena FC" title="Indirizzo" readonly/>
      </div>
      <!--utilzzare px-4 anche nel form login al posto di mx auto -->
      <div class="form-group row mt-4 mb-4 px-4 mx-auto">
        <label class="col-12" for="numerocarta">Numero della carta di credito</label>
        <input type="text" maxlength="19"
        class="field col-12" placeholder="XXXX XXXX XXXX XXXX" id="numerocarta"
        name="numerocarta" title="Inserisci il numero della tua carta" onkeypress="return onlyNumberKey(event)" required/>
        <script>formattaCarta();</script>
      </div>

      <div class="form-group row mt-4 mb-4 px-4 mx-auto">
        <label class="col-12" for="titolare">Nome del titolare della carta</label>
        <input type="text" class="field col-12" placeholder="Mario Rossi" id="titolare"
        name="titolare" title="Inserisci il nome del titolare" required/>
      </div>

      <div class="form-group row mt-4 mb-0 px-4 mx-auto justify-content-between">
        <label class="col-2" for="mesescadenza">Mese</label>
        <label class="col-3" for="annoscadenza">Anno</label>
        <label class="col-3" for="cvv">CVV</label>
      </div>
      <div class="form-group row mt-0 mb-4 px-4 mx-auto justify-content-between">
        <input class="col-2" type="text" maxlength="2" placeholder="MM" id="mesescadenza"
        name="mesescadenza" title="Inserisci il mese di scadenza" onkeypress="return onlyNumberKey(event)" required>
        <script>filtraMese();</script>

        <input class="col-3" type="text" maxlength="4" placeholder="AAAA" id="annoscadenza"
        name="annoscadenza" title="Inserisci l'anno di scadenza" onkeypress="return onlyNumberKey(event)" required>
        <script>filtraAnno();</script>

        <input class="col-3" type="password" maxlength="3" class="col-10 mx-auto" placeholder="CVV" id="cvv"
        name="cvv" title="Inserisci il codice di sicurezza" onkeypress="return onlyNumberKey(event)" required/>
      </div>
      <!--TODO implementare controlli con js per il numero della carta, la scadenza e il CVV-->
      <div class="row mt-3 mb-3">
        <button type="submit" class="btn btn-primary col-10 mx-auto p-2">Procedi all'acquisto</button>
      </div>
    </form>
  </div>
</div>
