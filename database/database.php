<?php
class DatabaseHelper{
    private $db;

    public function __construct($servername, $username, $password, $dbname, $port){
        $this->db = new mysqli($servername, $username, $password, $dbname, $port);
        if ($this->db->connect_error) {
            die("Connection failed: " . $db->connect_error);
        }
    }


    public function insertArticle( $nome, $descr, $prezzo, $img, $quantity, $autore, $anno){
        $query = "INSERT INTO articolo (nome, descr, prezzo, img, quantity, visible, autore, anno) VALUES ( ?, ?, ?, ?, ?, 1, ?, ?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ssisisi', $nome, $descr, $prezzo, $img, $quantity, $autore, $anno);
        $stmt->execute();

        return $stmt->insert_id;
    }

    public function getArticle(){
      $query = "SELECT * FROM articolo";
      $stmt = $this->db->prepare($query);
      $stmt->execute();
      $result = $stmt->get_result();

      return $result->fetch_all(MYSQLI_ASSOC);
    }

//-------- questa viene usata al momento dell' inserimento dell' articolo e basta -------
    public function getArticleId($nome, $descr, $prezzo, $img, $autore, $anno){
      $query = "SELECT idarticolo FROM articolo WHERE nome =? AND descr=? AND prezzo=? AND img=? AND autore=? AND anno=?";
      $stmt = $this->db->prepare($query);
      $stmt->bind_param('ssissi', $nome, $descr, $prezzo, $img, $autore, $anno);
      $stmt->execute();
      $result = $stmt->get_result();

      return $result->fetch_all(MYSQLI_ASSOC);
    }
//----------------------------------------------------------
//------- usabile in carrello.php --------------
public function getArticleById($id){
  $query = "SELECT * FROM articolo WHERE idarticolo=?";
  $stmt = $this->db->prepare($query);
  $stmt->bind_param('i', $id);
  $stmt->execute();
  $result = $stmt->get_result();

  return $result->fetch_all(MYSQLI_ASSOC);
}
//------------------------------------

    public function getImgArticle($idarticolo){
      $query = "SELECT img FROM articolo WHERE idarticolo = ?";
      $stmt = $this->db->prepare($query);
      $stmt->bind_param('i',$idarticolo);
      $stmt->execute();
      $result = $stmt->get_result();

      return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getArticleOfSeller($id){
      $query = "SELECT * FROM articolo artic ,userarticles usrart WHERE artic.idarticolo = usrart.productid  AND usrart.userid = ? ORDER BY idarticolo DESC";
      $stmt = $this->db->prepare($query);
      $stmt->bind_param('i',$id);
      $stmt->execute();
      $result = $stmt->get_result();

      return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getCartByUserId($id){
      $query = "SELECT * FROM cart , articolo WHERE articolo.idarticolo = cart.articleid AND  cart.userid = ? ";
      $stmt = $this->db->prepare($query);
      $stmt->bind_param('i',$id);
      $stmt->execute();
      $result = $stmt->get_result();

      return $result->fetch_all(MYSQLI_ASSOC);
    }

// ------------ Operazioni per Login e Registrazione utente ------------

    //Controlla se un determinato username è presente o meno nel database
    public function checkUsername($username){
      $query = "SELECT username FROM utenti WHERE username = ?";
      $stmt = $this->db->prepare($query);
      $stmt->bind_param('s',$username);
      $stmt->execute();
      $result = $stmt->get_result();
      $tmp = $result->fetch_assoc();
      if($tmp != NULL){
        return TRUE;
      }
      return FALSE;
    }

    //Serve per processare le informazioni di Login
    function processLogin($username, $password){
      //Recupero dal database tutti i dati dell'utente
      $query = "SELECT idutente, username, password, nome, tipoutente FROM utenti WHERE username = ?";
      $stmt = $this->db->prepare($query);
      $stmt->bind_param('s',$username);
      $stmt->execute();
      $result = $stmt->get_result();  //Result è un oggetto con un campo num_rows. Se è 1 allora ho trovato un utente

      //Inserisco in una variabile temporanea i dati recuperati dal database e ne prendo la password salata
      $tmp = $result->fetch_assoc();
      $hashed_password = $tmp["password"];
      if($result->num_rows == 1){
        //Ho trovato un utente con l'username fornito
        if (password_verify($password, $hashed_password)){
          //La password combacia
          return $tmp;
        }
      }
      //Username, Password o tipo utente erano errati
      return NULL;
    }

    //Inserisce i dati di un nuovo utente nel database
    public function insertUser($name, $username, $password, $type){
      $query = "INSERT INTO utenti (username, password, nome, tipoutente) VALUES (?, ?, ?, ?)";
      $stmt = $this->db->prepare($query);

      //Procedo con la salatura della password fornita
      $hashed_password = password_hash($password, PASSWORD_DEFAULT);
      $stmt->bind_param('ssss', $username, $hashed_password, $name, $type);
      $stmt->execute();
      if($stmt){
        return TRUE;
      }
      return FALSE;
    }


//da scrivere è solo un copia e incolla da quella sopra !!!!!!!!!!!
    public function insertArticleOfUser($userid,$articleid){
      $query = "INSERT INTO userarticles (userid,productid) VALUES ( ?, ?)";
      $stmt = $this->db->prepare($query);
      $stmt->bind_param('ii',$userid,$articleid);
      $stmt->execute();
      return true;
    }

    public function addToCart($userid,$idarticolo,$price, $quantity){
      $query = "INSERT INTO cart (userid, articleid, price, quantità) VALUES ( ?, ?, ?, ?)";
      $stmt = $this->db->prepare($query);
      $stmt->bind_param('iiii',$userid,$idarticolo,$price, $quantity);
      $stmt->execute();

    }

//------------ operazioni di delete -----

    public function deleteArticle($idarticolo){
      $query = "DELETE FROM articolo WHERE idarticolo = ?";
      $stmt = $this->db->prepare($query);
      $stmt->bind_param('i',$idarticolo);
      $stmt->execute();
      return true;
    }

    public function deleteFromUserarticles($id){
      $query = "DELETE FROM userarticles WHERE productid = ?";
      $stmt = $this->db->prepare($query);
      $stmt->bind_param('i',$id);
      $stmt->execute();
      return true;
    }

    public function removefromCart($userid,$order){
      $query = "DELETE FROM cart WHERE userid = ? AND orderid = ?";
      $stmt = $this->db->prepare($query);
      $stmt->bind_param('ii',$userid,$order);
      $stmt->execute();
      return true;
    }

//------------ Database di Davide Barbaro -----

    public function getArticleSpecific($name){
      $query = "SELECT * FROM articolo, userarticles WHERE nome LIKE '%$name%' AND productid = idarticolo";
      $stmt = $this->db->prepare($query);
      $stmt->execute();
      $result = $stmt->get_result();
      return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function addOrder($client, $article, $quantity){
      $query = "INSERT INTO ordini (clientid, articleid, status, quant) VALUES ( ?, ?, 1, ?)";
      $stmt = $this->db->prepare($query);
      $stmt->bind_param('iii',$client, $article, $quantity);
      $stmt->execute();

      return $stmt->insert_id;
    }

    public function getRandomPosts($n){
        $stmt = $this->db->prepare("SELECT * FROM articolo, userarticles WHERE productid = idarticolo ORDER BY RAND() LIMIT ?");
        $stmt->bind_param('i',$n);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getSells($id){
      $query = "SELECT orderid, idarticolo, nome, prezzo, img, status, quant, clientid FROM userarticles AS u JOIN ordini AS n ON u.productid = n.articleid JOIN articolo AS art ON art.idarticolo = u.productid WHERE u.userid = ? OR n.clientid = ? ORDER BY orderid DESC";
      $stmt = $this->db->prepare($query);
      $stmt->bind_param('ii',$id,$id);
      $stmt->execute();
      $result = $stmt->get_result();

      return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getBought($id){
      $query = "SELECT orderid, idarticolo, nome, prezzo, img, status, quant, clientid FROM ordini AS n JOIN articolo AS art ON art.idarticolo = n.articleid WHERE n.clientid = ? ORDER BY orderid DESC";
      $stmt = $this->db->prepare($query);
      $stmt->bind_param('i',$id);
      $stmt->execute();
      $result = $stmt->get_result();

      return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function statusUpdate($id, $status){
      $query = "UPDATE ordini SET status = ? WHERE orderid = ?";
      $stmt = $this->db->prepare($query);
      $stmt->bind_param('ii', $status, $id);
      return $stmt->execute();
    }

    public function getArticleVisible(){
      $query = "SELECT * FROM articolo WHERE visible = 1";
      $stmt = $this->db->prepare($query);
      $stmt->execute();
      $result = $stmt->get_result();

      return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function setArticleInvisibile($id){
      $query = "UPDATE articolo SET visible = 0 WHERE idarticolo = ? AND quantity = 0";
      $stmt = $this->db->prepare($query);
      $stmt->bind_param('i', $id);
      return $stmt->execute();
    }

    public function setArticleVisibile($id){
      $query = "UPDATE articolo SET visible = 1 WHERE idarticolo = ?";
      $stmt = $this->db->prepare($query);
      $stmt->bind_param('i', $id);
      return $stmt->execute();
    }

    public function deleteOrdini($id){
      $query = "DELETE FROM ordini WHERE orderid = ?";
      $stmt = $this->db->prepare($query);
      $stmt->bind_param('i', $id);
      $stmt->execute();
      return true;
    }

    //---------- modifica visualizzazione di articoli inseriti nella tabella ------//

    public function decreaseQuantity($idarticolo,$quantity){
    /*$query = "UPDATE articolo SET showing = 2 WHERE idarticolo = ?";*/
      $query = "UPDATE articolo SET quantity = (quantity-?)  WHERE idarticolo = ?";
      $stmt = $this->db->prepare($query);
      $stmt->bind_param('ii',$quantity,$idarticolo);
      $stmt->execute();
    }

    public function incQuantita($idarticolo,$quantity){
    /*$query = "UPDATE articolo SET showing = 1 WHERE idarticolo = ?";*/
      $query = "UPDATE articolo SET quantity = (quantity+?)  WHERE idarticolo = ?";
      $stmt = $this->db->prepare($query);
      $stmt->bind_param('ii',$quantity,$idarticolo);
      $stmt->execute();
    }

    public function decreaseQuantityCart($order,$quantity){
    /*$query = "UPDATE articolo SET showing = 2 WHERE idarticolo = ?";*/
      $query = "UPDATE cart SET quantità = (quantità-?)  WHERE orderid = ?";
      $stmt = $this->db->prepare($query);
      $stmt->bind_param('ii',$quantity,$order);
      $stmt->execute();
      return $stmt->get_result();
    }

    public function incQuantitaCart($order,$quantity){
    /*$query = "UPDATE articolo SET showing = 1 WHERE idarticolo = ?";*/
      $query = "UPDATE cart SET quantità = (quantità+?)  WHERE orderid = ?";
      $stmt = $this->db->prepare($query);
      $stmt->bind_param('ii',$quantity,$order);
      $stmt->execute();
    }

    public function SetQuantCart($order,$quantity){
    /*$query = "UPDATE articolo SET showing = 1 WHERE idarticolo = ?";*/
      $query = "UPDATE cart SET quantità = ?  WHERE orderid = ?";
      $stmt = $this->db->prepare($query);
      $stmt->bind_param('ii',$quantity,$order);
      $stmt->execute();
    }

    public function getCartItemByUserId($id,$order){
      $query = "SELECT * FROM cart , articolo WHERE articolo.idarticolo = cart.articleid AND  cart.userid = ? AND cart.orderid = ?";
      $stmt = $this->db->prepare($query);
      $stmt->bind_param('ii',$id,$order);
      $stmt->execute();
      $result = $stmt->get_result();

      return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getNotification($id){
      $query = "SELECT * FROM notifiche, articolo WHERE userid = ? AND idarticolo = articleid AND notifiedC = 0 AND type != 1 ORDER BY notifyid DESC";
      $stmt = $this->db->prepare($query);
      $stmt->bind_param('i',$id);
      $stmt->execute();
      $result = $stmt->get_result();

      return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getNotificationAdmin($id){
      $query = "SELECT * FROM notifiche JOIN articolo ON articleid = idarticolo WHERE (sellerid = ? AND notifiedS = 0) OR (userid = ? AND notifiedC = 0 AND type != 1) ORDER BY notifyid DESC";
      $stmt = $this->db->prepare($query);
      $stmt->bind_param('ii',$id,$id);
      $stmt->execute();
      $result = $stmt->get_result();

      return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getNotificationAll($id){
      $query = "SELECT * FROM notifiche, articolo WHERE userid = ? AND idarticolo = articleid ORDER BY notifyid DESC";
      $stmt = $this->db->prepare($query);
      $stmt->bind_param('i',$id);
      $stmt->execute();
      $result = $stmt->get_result();

      return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getNotificationAdminAll($id){
      $query = "SELECT * FROM utenti, notifiche, articolo WHERE (sellerid = ? OR (userid = ? AND type != 1)) AND idarticolo = articleid AND notifiche.userid = utenti.idutente ORDER BY notifyid DESC";
      $stmt = $this->db->prepare($query);
      $stmt->bind_param('ii',$id,$id);
      $stmt->execute();
      $result = $stmt->get_result();

      return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function addNotification($client, $seller, $article, $type, $quantity){
      $query = "INSERT INTO notifiche (userid, sellerid, articleid, type, notifiedC, notifiedS, quantitya) VALUES ( ?, ?, ?, ?, 0, 0, ?)";
      $stmt = $this->db->prepare($query);
      $stmt->bind_param('iiiii',$client, $seller, $article, $type, $quantity);
      $stmt->execute();

      return $stmt->insert_id;
    }

    public function getSellerOfArticle($article){
      $query = "SELECT userid FROM userarticles WHERE productid = ?";
      $stmt = $this->db->prepare($query);
      $stmt->bind_param('i',$article);
      $stmt->execute();
      $result = $stmt->get_result();

      return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getSaldo($id){
      $query = "SELECT saldo FROM utenti WHERE idutente = ?";
      $stmt = $this->db->prepare($query);
      $stmt->bind_param('i',$id);
      $stmt->execute();
      $result = $stmt->get_result();

      return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function NotificationUpdate($id){
      $query = "UPDATE notifiche SET notifiedC = 1 WHERE userid = ?";
      $stmt = $this->db->prepare($query);
      $stmt->bind_param('i', $id);
      return $stmt->execute();
    }

    public function NotificationUpdateAdmin($id){
      $query = "UPDATE notifiche SET notifiedS = 1 WHERE sellerid = ?";
      $stmt = $this->db->prepare($query);
      $stmt->bind_param('i', $id);
      return $stmt->execute();
    }

    public function RicaricaSaldo($id, $saldo){
      $query = "UPDATE utenti SET saldo = (saldo+?) WHERE idutente = ?";
      $stmt = $this->db->prepare($query);
      $stmt->bind_param('ii',$saldo, $id);
      return $stmt->execute();
    }

    public function NotificationCancel($id){
      $query = "DELETE FROM notifiche WHERE articleid = ?";
      $stmt = $this->db->prepare($query);
      $stmt->bind_param('i', $id);
      $stmt->execute();
      return true;
    }

    public function FindOrder($id){
      $query = "SELECT orderid FROM ordini WHERE articleid = ?";
      $stmt = $this->db->prepare($query);
      $stmt->bind_param('i', $id);
      $stmt->execute();
      $result = $stmt->get_result();

      return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function FindCart($id){
      $query = "SELECT orderid FROM cart WHERE articleid = ?";
      $stmt = $this->db->prepare($query);
      $stmt->bind_param('i', $id);
      $stmt->execute();
      $result = $stmt->get_result();

      return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function deleteArticleFromCart($id){
      $query = "DELETE FROM cart WHERE orderid = ?";
      $stmt = $this->db->prepare($query);
      $stmt->bind_param('i',$id);
      $stmt->execute();
      return true;
    }

/*

    public function getRandomPosts($n){
        $stmt = $this->db->prepare("SELECT idarticolo, titoloarticolo, imgarticolo FROM articolo ORDER BY RAND() LIMIT ?");
        $stmt->bind_param('i',$n);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getCategories(){
        $stmt = $this->db->prepare("SELECT * FROM categoria");
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getCategoryById($idcategory){
        $stmt = $this->db->prepare("SELECT nomecategoria FROM categoria WHERE idcategoria=?");
        $stmt->bind_param('i',$idcategory);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getPosts($n=-1){
        $query = "SELECT idarticolo, titoloarticolo, imgarticolo, anteprimaarticolo, dataarticolo, nome FROM articolo, autore WHERE autore=idautore ORDER BY dataarticolo DESC";
        if($n > 0){
            $query .= " LIMIT ?";
        }
        $stmt = $this->db->prepare($query);
        if($n > 0){
            $stmt->bind_param('i',$n);
        }
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getPostById($id){
        $query = "SELECT idarticolo, titoloarticolo, imgarticolo, testoarticolo, dataarticolo, nome FROM articolo, autore WHERE idarticolo=? AND autore=idautore";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i',$id);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getPostByCategory($idcategory){
        $query = "SELECT idarticolo, titoloarticolo, imgarticolo, anteprimaarticolo, dataarticolo, nome FROM articolo, autore, articolo_ha_categoria WHERE categoria=? AND autore=idautore AND idarticolo=articolo";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i',$idcategory);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getPostByIdAndAuthor($id, $idauthor){
        $query = "SELECT idarticolo, anteprimaarticolo, titoloarticolo, imgarticolo, testoarticolo, dataarticolo, (SELECT GROUP_CONCAT(categoria) FROM articolo_ha_categoria WHERE articolo=idarticolo GROUP BY articolo) as categorie FROM articolo WHERE idarticolo=? AND autore=?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ii',$id, $idauthor);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getPostByAuthorId($id){
        $query = "SELECT idarticolo, titoloarticolo, imgarticolo FROM articolo WHERE autore=?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i',$id);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function insertArticle($titoloarticolo, $testoarticolo, $anteprimaarticolo, $dataarticolo, $imgarticolo, $autore){
        $query = "INSERT INTO articolo (titoloarticolo, testoarticolo, anteprimaarticolo, dataarticolo, imgarticolo, autore) VALUES (?, ?, ?, ?, ?, ?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('sssssi',$titoloarticolo, $testoarticolo, $anteprimaarticolo, $dataarticolo, $imgarticolo, $autore);
        $stmt->execute();

        return $stmt->insert_id;
    }

    public function updateArticleOfAuthor($idarticolo, $titoloarticolo, $testoarticolo, $anteprimaarticolo, $imgarticolo, $autore){
        $query = "UPDATE articolo SET titoloarticolo = ?, testoarticolo = ?, anteprimaarticolo = ?, imgarticolo = ? WHERE idarticolo = ? AND autore = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ssssii',$titoloarticolo, $testoarticolo, $anteprimaarticolo, $imgarticolo, $idarticolo, $autore);

        return $stmt->execute();
    }

    public function deleteArticleOfAuthor($idarticolo, $autore){
        $query = "DELETE FROM articolo WHERE idarticolo = ? AND autore = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ii',$idarticolo, $autore);
        $stmt->execute();
        var_dump($stmt->error);
        return true;
    }

    public function insertCategoryOfArticle($articolo, $categoria){
        $query = "INSERT INTO articolo_ha_categoria (articolo, categoria) VALUES (?, ?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ii',$articolo, $categoria);
        return $stmt->execute();
    }

    public function deleteCategoryOfArticle($articolo, $categoria){
        $query = "DELETE FROM articolo_ha_categoria WHERE articolo = ? AND categoria = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ii',$articolo, $categoria);
        return $stmt->execute();
    }

    public function deleteCategoriesOfArticle($articolo){
        $query = "DELETE FROM articolo_ha_categoria WHERE articolo = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i',$articolo);
        return $stmt->execute();
    }

    public function getAuthors(){
        $query = "SELECT username, nome, GROUP_CONCAT(DISTINCT nomecategoria) as argomenti FROM categoria, articolo, autore, articolo_ha_categoria WHERE idarticolo=articolo AND categoria=idcategoria AND autore=idautore AND attivo=1 GROUP BY username, nome";
        $stmt = $this->db->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function checkLogin($username, $password){
        $query = "SELECT idautore, username, nome FROM autore WHERE attivo=1 AND username = ? AND password = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ss',$username, $password);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }
*/
}
?>
