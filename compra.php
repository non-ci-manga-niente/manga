<?php
require_once 'bootstrap.php';
if(!isset($_SESSION["id"])){
  header("location: login.php");
}
$userid= $_SESSION["id"];
$incart = $dbh->getCartByUserId($userid);

if(empty($incart)){
  setcookie("found", "Il carrello è vuoto!", time()+ 60,'/');
  header("location: index.php");
}
else {
  $errore = 0;
  if($errore == 0){
    foreach ($incart as $article) {
      $art = $dbh->getArticleById($article["articleid"]);
      if($article["quantità"] > $art[0]["quantity"]){
        $errore = 1;
      }
      if ($errore == 0) {
        $seller = $dbh->getSellerOfArticle($article["articleid"]);
        if(empty($seller)){
          $errore = 1;
          $dbh->removefromCart($userid,$article["orderid"]);
        }
        else {
          $dbh->addOrder($article["userid"], $article["articleid"], $article["quantità"]);
          $dbh->addNotification($article["userid"], $seller[0]["userid"], $article["articleid"], 0, $article["quantità"]);
          $dbh->removefromCart($userid,$article["orderid"]);
          $dbh->decreaseQuantity($article["articleid"], $article["quantità"]);
          $articolo = $dbh->getArticleById($article["articleid"]);
          if($articolo[0]["quantity"] == 0){
            $dbh->addNotification($article["userid"], $seller[0]["userid"], $article["articleid"], 1, $article["quantità"]);
          }
        }
      }
    }
  }
}
$templateParams["titolo"] = "Acquisto effettuato";
$templateParams["nome"] = 'compra-page.php';
require 'template/base.php';
?>
